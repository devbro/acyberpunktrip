// GENERATED AUTOMATICALLY FROM 'Assets/InputActions/PlayerControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerControls"",
    ""maps"": [
        {
            ""name"": ""Player"",
            ""id"": ""5c371735-479a-44e8-8ed4-a234f160b382"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Button"",
                    ""id"": ""f48802cd-17a1-46be-a27e-2fa8e56a1433"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Aim"",
                    ""type"": ""Button"",
                    ""id"": ""c35738c2-7064-4149-a47c-8afa01ce74f0"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RangedAttack"",
                    ""type"": ""Button"",
                    ""id"": ""5b9a3df5-a1c9-4d5b-bfb5-0f833dcc02b4"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Dash"",
                    ""type"": ""Button"",
                    ""id"": ""d9c815db-044e-4da4-881c-20042388e0cc"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""OpenPauseMenu"",
                    ""type"": ""Button"",
                    ""id"": ""d99a86e8-8362-46be-9c5f-bc867ade5386"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""StartDeadEye"",
                    ""type"": ""Button"",
                    ""id"": ""338ff8f0-35ba-42de-82c0-70115bc1f9a7"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ExecuteDeadEye"",
                    ""type"": ""Button"",
                    ""id"": ""45b2f8ac-6ebc-4efa-b49b-1ab62ce599c2"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MarkEnemyForDeadEye"",
                    ""type"": ""Button"",
                    ""id"": ""70d01b73-a09b-4743-be52-cbbbdfed0060"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ChangeWeapon"",
                    ""type"": ""Button"",
                    ""id"": ""689563ad-39fb-4789-b450-993a3b3de27c"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MeleeAttack"",
                    ""type"": ""Button"",
                    ""id"": ""7478391c-34ee-46ce-b8b9-8b1ccb42e8a9"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ExecuteSwordDance"",
                    ""type"": ""Button"",
                    ""id"": ""530c7521-c6b8-43e4-93dc-a56133252696"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""21a84372-62d8-4ccc-87dd-382c0fc610b9"",
                    ""path"": ""<XInputController>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e6950bc3-234d-4724-8c16-536be9697395"",
                    ""path"": ""<XInputController>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RangedAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""266df243-20ac-4f24-93e1-04755d0ddedc"",
                    ""path"": ""<XInputController>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""24af1b8e-2f80-4c23-b9cb-ad2e52eb3abe"",
                    ""path"": ""<XInputController>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""OpenPauseMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f592e44d-719d-4e62-9ab2-b8e1af99beb0"",
                    ""path"": ""<XInputController>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""StartDeadEye"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""84513906-991c-47b5-a3eb-934b59601230"",
                    ""path"": ""<XInputController>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ExecuteDeadEye"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1627dc7c-23ba-492d-a0da-58eb09746bf7"",
                    ""path"": ""<XInputController>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MarkEnemyForDeadEye"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""155fd077-05fe-41b6-9d14-e292e0c303b4"",
                    ""path"": ""<XInputController>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangeWeapon"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e034ba56-32e7-4b51-908c-3af6158630a2"",
                    ""path"": ""<XInputController>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MeleeAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3ddf04b8-d5db-42db-b493-de357c06ad8e"",
                    ""path"": ""<XInputController>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ExecuteSwordDance"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a1517719-d75f-468d-95f2-c85855b38078"",
                    ""path"": ""<XInputController>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Aim"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""mouseAndKey"",
            ""id"": ""fafcfa25-b3fd-4b50-80c8-3ef898679839"",
            ""actions"": [
                {
                    ""name"": ""RangedAttack"",
                    ""type"": ""Button"",
                    ""id"": ""db2e860c-8bdb-44f1-bf76-30de0110bd5b"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Move"",
                    ""type"": ""Button"",
                    ""id"": ""d5585518-b7b0-45ac-bf09-8c4803757817"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Dash"",
                    ""type"": ""Button"",
                    ""id"": ""0d71d238-30a0-4ba9-8d94-c2f8ac758577"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Aim"",
                    ""type"": ""PassThrough"",
                    ""id"": ""cc1ee66a-f322-4715-8a18-9c6747407376"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MeleeAttack"",
                    ""type"": ""Button"",
                    ""id"": ""25c76855-bcdb-4c7a-8367-20df543c9e22"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""StartDeadEye"",
                    ""type"": ""Button"",
                    ""id"": ""c834f205-8bb9-4373-8174-7f79141f1269"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ExecuteDeadEye"",
                    ""type"": ""Button"",
                    ""id"": ""9dc06206-6840-46c6-a2e5-8435ef7ace58"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MarkEnemyForDeadEye"",
                    ""type"": ""Button"",
                    ""id"": ""427d35a2-fbef-4db8-9769-008651cda6f9"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ExecuteSwordDance"",
                    ""type"": ""Button"",
                    ""id"": ""253b0f01-5d49-4a81-a08c-8e8d5ca81219"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ChangeWeapon"",
                    ""type"": ""Button"",
                    ""id"": ""611d52ff-4aa1-401a-a258-616f1d31e7a2"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""3030729f-41c4-4762-af5d-d439dcb0bc20"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RangedAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""e0623d5f-f6de-42ef-a766-8aafa83a7547"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""a8a29a39-4047-47bd-9fa7-e68320d918fd"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""376203d2-3f8c-4079-8dcb-93af7cb51b6c"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""6b4880d0-7fda-4059-8cec-615998b5477b"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""18f58003-fa15-4547-8c1a-da3cf2ef2451"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""7773adc0-2497-4314-ae7b-d2e1597d5d17"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d2ec9a11-6976-4567-8d75-96dc2c835af2"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Aim"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""398b256d-6eac-472a-8550-37f2634675ae"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MeleeAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fcb2bd04-7792-4aaa-a46d-6f3ebb6fc14d"",
                    ""path"": ""<Keyboard>/#(Q)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""StartDeadEye"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5d5ce71c-f824-41bd-82c4-daa66585c58b"",
                    ""path"": ""<Keyboard>/#(Q)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ExecuteDeadEye"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ce24efe4-ae0c-4762-8188-aadcebaf4fd4"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MarkEnemyForDeadEye"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9111e38b-3891-4f32-a050-fef8f2225802"",
                    ""path"": ""<Keyboard>/#(E)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ExecuteSwordDance"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""713a2029-d555-4271-872e-838679c6c086"",
                    ""path"": ""<Mouse>/scroll/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangeWeapon"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Player
        m_Player = asset.FindActionMap("Player", throwIfNotFound: true);
        m_Player_Move = m_Player.FindAction("Move", throwIfNotFound: true);
        m_Player_Aim = m_Player.FindAction("Aim", throwIfNotFound: true);
        m_Player_RangedAttack = m_Player.FindAction("RangedAttack", throwIfNotFound: true);
        m_Player_Dash = m_Player.FindAction("Dash", throwIfNotFound: true);
        m_Player_OpenPauseMenu = m_Player.FindAction("OpenPauseMenu", throwIfNotFound: true);
        m_Player_StartDeadEye = m_Player.FindAction("StartDeadEye", throwIfNotFound: true);
        m_Player_ExecuteDeadEye = m_Player.FindAction("ExecuteDeadEye", throwIfNotFound: true);
        m_Player_MarkEnemyForDeadEye = m_Player.FindAction("MarkEnemyForDeadEye", throwIfNotFound: true);
        m_Player_ChangeWeapon = m_Player.FindAction("ChangeWeapon", throwIfNotFound: true);
        m_Player_MeleeAttack = m_Player.FindAction("MeleeAttack", throwIfNotFound: true);
        m_Player_ExecuteSwordDance = m_Player.FindAction("ExecuteSwordDance", throwIfNotFound: true);
        // mouseAndKey
        m_mouseAndKey = asset.FindActionMap("mouseAndKey", throwIfNotFound: true);
        m_mouseAndKey_RangedAttack = m_mouseAndKey.FindAction("RangedAttack", throwIfNotFound: true);
        m_mouseAndKey_Move = m_mouseAndKey.FindAction("Move", throwIfNotFound: true);
        m_mouseAndKey_Dash = m_mouseAndKey.FindAction("Dash", throwIfNotFound: true);
        m_mouseAndKey_Aim = m_mouseAndKey.FindAction("Aim", throwIfNotFound: true);
        m_mouseAndKey_MeleeAttack = m_mouseAndKey.FindAction("MeleeAttack", throwIfNotFound: true);
        m_mouseAndKey_StartDeadEye = m_mouseAndKey.FindAction("StartDeadEye", throwIfNotFound: true);
        m_mouseAndKey_ExecuteDeadEye = m_mouseAndKey.FindAction("ExecuteDeadEye", throwIfNotFound: true);
        m_mouseAndKey_MarkEnemyForDeadEye = m_mouseAndKey.FindAction("MarkEnemyForDeadEye", throwIfNotFound: true);
        m_mouseAndKey_ExecuteSwordDance = m_mouseAndKey.FindAction("ExecuteSwordDance", throwIfNotFound: true);
        m_mouseAndKey_ChangeWeapon = m_mouseAndKey.FindAction("ChangeWeapon", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Player
    private readonly InputActionMap m_Player;
    private IPlayerActions m_PlayerActionsCallbackInterface;
    private readonly InputAction m_Player_Move;
    private readonly InputAction m_Player_Aim;
    private readonly InputAction m_Player_RangedAttack;
    private readonly InputAction m_Player_Dash;
    private readonly InputAction m_Player_OpenPauseMenu;
    private readonly InputAction m_Player_StartDeadEye;
    private readonly InputAction m_Player_ExecuteDeadEye;
    private readonly InputAction m_Player_MarkEnemyForDeadEye;
    private readonly InputAction m_Player_ChangeWeapon;
    private readonly InputAction m_Player_MeleeAttack;
    private readonly InputAction m_Player_ExecuteSwordDance;
    public struct PlayerActions
    {
        private @PlayerControls m_Wrapper;
        public PlayerActions(@PlayerControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_Player_Move;
        public InputAction @Aim => m_Wrapper.m_Player_Aim;
        public InputAction @RangedAttack => m_Wrapper.m_Player_RangedAttack;
        public InputAction @Dash => m_Wrapper.m_Player_Dash;
        public InputAction @OpenPauseMenu => m_Wrapper.m_Player_OpenPauseMenu;
        public InputAction @StartDeadEye => m_Wrapper.m_Player_StartDeadEye;
        public InputAction @ExecuteDeadEye => m_Wrapper.m_Player_ExecuteDeadEye;
        public InputAction @MarkEnemyForDeadEye => m_Wrapper.m_Player_MarkEnemyForDeadEye;
        public InputAction @ChangeWeapon => m_Wrapper.m_Player_ChangeWeapon;
        public InputAction @MeleeAttack => m_Wrapper.m_Player_MeleeAttack;
        public InputAction @ExecuteSwordDance => m_Wrapper.m_Player_ExecuteSwordDance;
        public InputActionMap Get() { return m_Wrapper.m_Player; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerActions instance)
        {
            if (m_Wrapper.m_PlayerActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                @Aim.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAim;
                @Aim.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAim;
                @Aim.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAim;
                @RangedAttack.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRangedAttack;
                @RangedAttack.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRangedAttack;
                @RangedAttack.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRangedAttack;
                @Dash.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDash;
                @Dash.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDash;
                @Dash.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDash;
                @OpenPauseMenu.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnOpenPauseMenu;
                @OpenPauseMenu.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnOpenPauseMenu;
                @OpenPauseMenu.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnOpenPauseMenu;
                @StartDeadEye.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnStartDeadEye;
                @StartDeadEye.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnStartDeadEye;
                @StartDeadEye.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnStartDeadEye;
                @ExecuteDeadEye.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnExecuteDeadEye;
                @ExecuteDeadEye.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnExecuteDeadEye;
                @ExecuteDeadEye.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnExecuteDeadEye;
                @MarkEnemyForDeadEye.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMarkEnemyForDeadEye;
                @MarkEnemyForDeadEye.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMarkEnemyForDeadEye;
                @MarkEnemyForDeadEye.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMarkEnemyForDeadEye;
                @ChangeWeapon.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnChangeWeapon;
                @ChangeWeapon.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnChangeWeapon;
                @ChangeWeapon.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnChangeWeapon;
                @MeleeAttack.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMeleeAttack;
                @MeleeAttack.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMeleeAttack;
                @MeleeAttack.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMeleeAttack;
                @ExecuteSwordDance.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnExecuteSwordDance;
                @ExecuteSwordDance.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnExecuteSwordDance;
                @ExecuteSwordDance.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnExecuteSwordDance;
            }
            m_Wrapper.m_PlayerActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Aim.started += instance.OnAim;
                @Aim.performed += instance.OnAim;
                @Aim.canceled += instance.OnAim;
                @RangedAttack.started += instance.OnRangedAttack;
                @RangedAttack.performed += instance.OnRangedAttack;
                @RangedAttack.canceled += instance.OnRangedAttack;
                @Dash.started += instance.OnDash;
                @Dash.performed += instance.OnDash;
                @Dash.canceled += instance.OnDash;
                @OpenPauseMenu.started += instance.OnOpenPauseMenu;
                @OpenPauseMenu.performed += instance.OnOpenPauseMenu;
                @OpenPauseMenu.canceled += instance.OnOpenPauseMenu;
                @StartDeadEye.started += instance.OnStartDeadEye;
                @StartDeadEye.performed += instance.OnStartDeadEye;
                @StartDeadEye.canceled += instance.OnStartDeadEye;
                @ExecuteDeadEye.started += instance.OnExecuteDeadEye;
                @ExecuteDeadEye.performed += instance.OnExecuteDeadEye;
                @ExecuteDeadEye.canceled += instance.OnExecuteDeadEye;
                @MarkEnemyForDeadEye.started += instance.OnMarkEnemyForDeadEye;
                @MarkEnemyForDeadEye.performed += instance.OnMarkEnemyForDeadEye;
                @MarkEnemyForDeadEye.canceled += instance.OnMarkEnemyForDeadEye;
                @ChangeWeapon.started += instance.OnChangeWeapon;
                @ChangeWeapon.performed += instance.OnChangeWeapon;
                @ChangeWeapon.canceled += instance.OnChangeWeapon;
                @MeleeAttack.started += instance.OnMeleeAttack;
                @MeleeAttack.performed += instance.OnMeleeAttack;
                @MeleeAttack.canceled += instance.OnMeleeAttack;
                @ExecuteSwordDance.started += instance.OnExecuteSwordDance;
                @ExecuteSwordDance.performed += instance.OnExecuteSwordDance;
                @ExecuteSwordDance.canceled += instance.OnExecuteSwordDance;
            }
        }
    }
    public PlayerActions @Player => new PlayerActions(this);

    // mouseAndKey
    private readonly InputActionMap m_mouseAndKey;
    private IMouseAndKeyActions m_MouseAndKeyActionsCallbackInterface;
    private readonly InputAction m_mouseAndKey_RangedAttack;
    private readonly InputAction m_mouseAndKey_Move;
    private readonly InputAction m_mouseAndKey_Dash;
    private readonly InputAction m_mouseAndKey_Aim;
    private readonly InputAction m_mouseAndKey_MeleeAttack;
    private readonly InputAction m_mouseAndKey_StartDeadEye;
    private readonly InputAction m_mouseAndKey_ExecuteDeadEye;
    private readonly InputAction m_mouseAndKey_MarkEnemyForDeadEye;
    private readonly InputAction m_mouseAndKey_ExecuteSwordDance;
    private readonly InputAction m_mouseAndKey_ChangeWeapon;
    public struct MouseAndKeyActions
    {
        private @PlayerControls m_Wrapper;
        public MouseAndKeyActions(@PlayerControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @RangedAttack => m_Wrapper.m_mouseAndKey_RangedAttack;
        public InputAction @Move => m_Wrapper.m_mouseAndKey_Move;
        public InputAction @Dash => m_Wrapper.m_mouseAndKey_Dash;
        public InputAction @Aim => m_Wrapper.m_mouseAndKey_Aim;
        public InputAction @MeleeAttack => m_Wrapper.m_mouseAndKey_MeleeAttack;
        public InputAction @StartDeadEye => m_Wrapper.m_mouseAndKey_StartDeadEye;
        public InputAction @ExecuteDeadEye => m_Wrapper.m_mouseAndKey_ExecuteDeadEye;
        public InputAction @MarkEnemyForDeadEye => m_Wrapper.m_mouseAndKey_MarkEnemyForDeadEye;
        public InputAction @ExecuteSwordDance => m_Wrapper.m_mouseAndKey_ExecuteSwordDance;
        public InputAction @ChangeWeapon => m_Wrapper.m_mouseAndKey_ChangeWeapon;
        public InputActionMap Get() { return m_Wrapper.m_mouseAndKey; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MouseAndKeyActions set) { return set.Get(); }
        public void SetCallbacks(IMouseAndKeyActions instance)
        {
            if (m_Wrapper.m_MouseAndKeyActionsCallbackInterface != null)
            {
                @RangedAttack.started -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnRangedAttack;
                @RangedAttack.performed -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnRangedAttack;
                @RangedAttack.canceled -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnRangedAttack;
                @Move.started -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnMove;
                @Dash.started -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnDash;
                @Dash.performed -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnDash;
                @Dash.canceled -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnDash;
                @Aim.started -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnAim;
                @Aim.performed -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnAim;
                @Aim.canceled -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnAim;
                @MeleeAttack.started -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnMeleeAttack;
                @MeleeAttack.performed -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnMeleeAttack;
                @MeleeAttack.canceled -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnMeleeAttack;
                @StartDeadEye.started -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnStartDeadEye;
                @StartDeadEye.performed -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnStartDeadEye;
                @StartDeadEye.canceled -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnStartDeadEye;
                @ExecuteDeadEye.started -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnExecuteDeadEye;
                @ExecuteDeadEye.performed -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnExecuteDeadEye;
                @ExecuteDeadEye.canceled -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnExecuteDeadEye;
                @MarkEnemyForDeadEye.started -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnMarkEnemyForDeadEye;
                @MarkEnemyForDeadEye.performed -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnMarkEnemyForDeadEye;
                @MarkEnemyForDeadEye.canceled -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnMarkEnemyForDeadEye;
                @ExecuteSwordDance.started -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnExecuteSwordDance;
                @ExecuteSwordDance.performed -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnExecuteSwordDance;
                @ExecuteSwordDance.canceled -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnExecuteSwordDance;
                @ChangeWeapon.started -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnChangeWeapon;
                @ChangeWeapon.performed -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnChangeWeapon;
                @ChangeWeapon.canceled -= m_Wrapper.m_MouseAndKeyActionsCallbackInterface.OnChangeWeapon;
            }
            m_Wrapper.m_MouseAndKeyActionsCallbackInterface = instance;
            if (instance != null)
            {
                @RangedAttack.started += instance.OnRangedAttack;
                @RangedAttack.performed += instance.OnRangedAttack;
                @RangedAttack.canceled += instance.OnRangedAttack;
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Dash.started += instance.OnDash;
                @Dash.performed += instance.OnDash;
                @Dash.canceled += instance.OnDash;
                @Aim.started += instance.OnAim;
                @Aim.performed += instance.OnAim;
                @Aim.canceled += instance.OnAim;
                @MeleeAttack.started += instance.OnMeleeAttack;
                @MeleeAttack.performed += instance.OnMeleeAttack;
                @MeleeAttack.canceled += instance.OnMeleeAttack;
                @StartDeadEye.started += instance.OnStartDeadEye;
                @StartDeadEye.performed += instance.OnStartDeadEye;
                @StartDeadEye.canceled += instance.OnStartDeadEye;
                @ExecuteDeadEye.started += instance.OnExecuteDeadEye;
                @ExecuteDeadEye.performed += instance.OnExecuteDeadEye;
                @ExecuteDeadEye.canceled += instance.OnExecuteDeadEye;
                @MarkEnemyForDeadEye.started += instance.OnMarkEnemyForDeadEye;
                @MarkEnemyForDeadEye.performed += instance.OnMarkEnemyForDeadEye;
                @MarkEnemyForDeadEye.canceled += instance.OnMarkEnemyForDeadEye;
                @ExecuteSwordDance.started += instance.OnExecuteSwordDance;
                @ExecuteSwordDance.performed += instance.OnExecuteSwordDance;
                @ExecuteSwordDance.canceled += instance.OnExecuteSwordDance;
                @ChangeWeapon.started += instance.OnChangeWeapon;
                @ChangeWeapon.performed += instance.OnChangeWeapon;
                @ChangeWeapon.canceled += instance.OnChangeWeapon;
            }
        }
    }
    public MouseAndKeyActions @mouseAndKey => new MouseAndKeyActions(this);
    public interface IPlayerActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnAim(InputAction.CallbackContext context);
        void OnRangedAttack(InputAction.CallbackContext context);
        void OnDash(InputAction.CallbackContext context);
        void OnOpenPauseMenu(InputAction.CallbackContext context);
        void OnStartDeadEye(InputAction.CallbackContext context);
        void OnExecuteDeadEye(InputAction.CallbackContext context);
        void OnMarkEnemyForDeadEye(InputAction.CallbackContext context);
        void OnChangeWeapon(InputAction.CallbackContext context);
        void OnMeleeAttack(InputAction.CallbackContext context);
        void OnExecuteSwordDance(InputAction.CallbackContext context);
    }
    public interface IMouseAndKeyActions
    {
        void OnRangedAttack(InputAction.CallbackContext context);
        void OnMove(InputAction.CallbackContext context);
        void OnDash(InputAction.CallbackContext context);
        void OnAim(InputAction.CallbackContext context);
        void OnMeleeAttack(InputAction.CallbackContext context);
        void OnStartDeadEye(InputAction.CallbackContext context);
        void OnExecuteDeadEye(InputAction.CallbackContext context);
        void OnMarkEnemyForDeadEye(InputAction.CallbackContext context);
        void OnExecuteSwordDance(InputAction.CallbackContext context);
        void OnChangeWeapon(InputAction.CallbackContext context);
    }
}
