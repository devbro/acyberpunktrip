﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobMovement : MonoBehaviour
{
    [Range(0, 2)]
    [SerializeField]
    private float m_avoidanceRadius;
    private float m_squareAvoidance;

    private Vector3 vel = Vector3.zero;

    private void Start()
    {
        m_squareAvoidance = m_avoidanceRadius * m_avoidanceRadius;
    }

    public Vector3 Move(Transform mob, List<Transform> flock)
    {
        //return Vector3.SmoothDamp(mob.position, mob.position += Avoidance(mob, flock),ref vel, 0.3f);
        return mob.position += Avoidance(mob, flock);
    }

    public Vector3 Avoidance(Transform mob, List<Transform> flock)
    {
        if (flock.Count == 0)
            return Vector3.zero;

        Vector3 avoidanceMove = Vector3.zero;
        int nAvoid = 0;
        foreach (Transform member in flock)
        {
            if (member.gameObject.activeSelf && Vector3.SqrMagnitude(mob.position - member.position) < m_squareAvoidance)
            {
                nAvoid++;
                avoidanceMove += mob.position - member.position;
            }
        }
        if (nAvoid > 0)
            avoidanceMove /= nAvoid;

        return avoidanceMove;
    }
}
