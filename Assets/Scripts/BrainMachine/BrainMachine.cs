﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrainMachine : MonoBehaviour
{
    [Header("CURRENT STATE:")]
    [SerializeField]
    private BrainState currentState;

    [Header("ALL STATES:")]
    [SerializeField]
    private List<BrainState> allStates = new List<BrainState>();
    private readonly List<BrainState> listOfStates = new List<BrainState>();

    private Stack<BrainState> brainStateStack = new Stack<BrainState>();

    private void Awake()
    {
        InitializeStates();
    }

    private void Update()
    {
        Execute();
    }

    private void InitializeStates()
    {
        if (allStates.Count == 0)
            return;
        foreach (BrainState state in allStates)
        {
            var brainState = Instantiate(state);
            listOfStates.Add(brainState);
        }
        PushBrainState(State(0));
    }

    private void Execute()
    {
        if (currentState != null)
            currentState.Behavior();
    }

    public void SetUser(BrainUser user)
    {
        foreach (BrainState state in listOfStates)
        {
            state.SetUser(user);
        }
    }

    public void PushBrainState(BrainState state)
    {
        if (currentState != state)
        {
            brainStateStack.Push(state);
            currentState = brainStateStack.Peek();
        }
        else
            Debug.Log("Same state as current state");
    }

    public void PopBrainState()
    {
        if (brainStateStack.Count > 1)
        {
            brainStateStack.Pop();
            currentState = brainStateStack.Peek();
        }
        else
            Debug.Log("Can't pop first state");
    }

    public void PopAllBrainStates()
    {
        while (brainStateStack.Count > 1)
            brainStateStack.Pop();
    }

    public BrainState State(int number)
    {
        if (number > listOfStates.Count - 1)
            return listOfStates[0];
        else
            return listOfStates[number];
    }
}
