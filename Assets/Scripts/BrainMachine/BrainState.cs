﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BrainState : ScriptableObject
{    
    public abstract void Behavior();

    public abstract void SetUser(BrainUser user);

    protected void Awake()
    {
        m_currentState = State.Phase1;
    }

    protected void ResetStatePhase()
    {
        m_currentState = State.Phase1;
    }

    protected State m_currentState;
    protected enum State
    {
        Phase1,
        Phase2,
        Phase3,
        Phase4,
        Phase5,
        Phase6,
        Phase7,
        Phase8,
        Phase9,
        Phase10
    }
}
