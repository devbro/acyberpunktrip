﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NinjaAttackState", menuName = "ScriptableObjects/BrainStates/NinjaAttackState")]
public class NinjaAttackState : MobState
{
    public override void Behavior()
    {
        switch (m_currentState)
        {
            case State.Phase1:
                if (user.PlayerWithinAttackRange())
                {
                    user.MeleeAttack();
                }
                else
                {
                    user.SearchForPlayerPosition();
                    m_currentState = State.Phase2;
                }
                break;
            case State.Phase2:
                user.StartTimer();
                if (user.TimerEnded())
                {
                    ResetStatePhase();
                    user.ResetTimer();
                    user.PopBrainState();
                }
                break;
        }
    }
}
