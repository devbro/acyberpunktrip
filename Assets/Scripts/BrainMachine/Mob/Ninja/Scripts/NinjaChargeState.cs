﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NinjaChargeState", menuName = "ScriptableObjects/BrainStates/NinjaChargeState")]
public class NinjaChargeState : MobState
{
    public override void Behavior()
    {
        switch (m_currentState)
        {
            case State.Phase1:
                user.MoveToAttackPosition();
                if (user.AttackPositionReached())
                    m_currentState = State.Phase2;
                break;
            case State.Phase2:
                ResetStatePhase();
                user.PushBrainState(2);
                break;
        }
    }
}
