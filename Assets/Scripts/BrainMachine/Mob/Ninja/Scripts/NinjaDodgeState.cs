﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NinjaDodgeState", menuName = "ScriptableObjects/BrainStates/NinjaDodgeState")]
public class NinjaDodgeState : MobState
{
    public override void Behavior()
    {
        switch (m_currentState)
        {
            case State.Phase1:
                user.NewPositionFromBullet();
                m_currentState = State.Phase2;
                break;
            case State.Phase2:
                user.MoveToNewPosition();
                if (user.NewPositionReached())
                {
                    user.NewPositionFromPlayer();
                    m_currentState = State.Phase3;
                }
                break;
            case State.Phase3:
                user.MoveToNewPosition();
                if (user.NewPositionReached())
                {
                    ResetStatePhase();
                    user.PushBrainState(1);
                }
                break;
        }
    }
}
