﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NinjaIdleState", menuName = "ScriptableObjects/BrainStates/NinjaIdleState")]
public class NinjaIdleState : MobState
{
    public override void Behavior()
    {
        switch (m_currentState)
        {
            case State.Phase1:
                user.SearchForPlayerAndBullet();
                if (user.PlayerBulletFound())
                    m_currentState = State.Phase3;
                else if (user.PlayerFound())
                    m_currentState = State.Phase3;
                break;
            case State.Phase2:
                user.PushBrainState(1);
                break;
            case State.Phase3:
                user.PushBrainState(3);
                break;
        }
    }
}
