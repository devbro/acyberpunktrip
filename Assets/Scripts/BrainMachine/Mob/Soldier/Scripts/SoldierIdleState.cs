﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SoldierIdleState", menuName = "ScriptableObjects/BrainStates/SoldierIdleState")]
public class SoldierIdleState : MobState
{
    public override void Behavior()
    {
        switch (m_currentState)
        {
            case State.Phase1:
                user.SearchForPlayer();
                if (user.PlayerBulletFound() || user.PlayerFound())
                    m_currentState = State.Phase2;
                break;
            case State.Phase2:
                ResetStatePhase();
                user.SetRandomAttackPosition();
                user.PushBrainState(1);
                break;
        }
    }
}
