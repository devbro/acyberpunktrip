﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SoldierShootState", menuName = "ScriptableObjects/BrainStates/SoldierShootState")]
public class SoldierShootState : MobState
{
    public override void Behavior()
    {
        switch (m_currentState)
        {
            case State.Phase1:
                user.RangedAttack();
                if (user.DistanceToPlayer() > user.AttackRange())
                {
                    user.PopBrainState();
                }
                break;
            case State.Phase2:

                break;
        }
    }
}
