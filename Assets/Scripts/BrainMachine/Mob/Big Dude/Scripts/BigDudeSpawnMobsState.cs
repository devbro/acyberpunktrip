﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BigDudeSpawnMobsState", menuName = "ScriptableObjects/BrainStates/BigDudeSpawnMobsState")]
public class BigDudeSpawnMobsState : MobState
{
    public override void Behavior()
    {
        switch (m_currentState)
        {
            case State.Phase1:
                user.SpawnMobs();
                m_currentState = State.Phase2;
                break;
            case State.Phase2:
                break;
        }
    }
}
