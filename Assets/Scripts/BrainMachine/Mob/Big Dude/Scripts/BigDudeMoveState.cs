﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BigDudeMoveState", menuName = "ScriptableObjects/BrainStates/BigDudeMoveState")]
public class BigDudeMoveState : MobState
{
    public override void Behavior()
    {
        switch (m_currentState)
        {
            case State.Phase1:
                user.MoveToPlayerPosition();
                if (user.PlayerPositionReached())
                    m_currentState = State.Phase2;
                else if (user.GetCurrentHealth() <= 10)
                    user.PushBrainState(3);
                break;
            case State.Phase2:
                ResetStatePhase();
                user.PushBrainState(2);
                break;
        }
    }

    // NOT WORKING
    //protected override void Start()
    //{
    //    base.Start();
    //    user.EntityIsDead += Spawn;
    //}

    //private void Spawn()
    //{
    //    user.SpawnMobs();
    //}
}
