﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BigDudeAttackState", menuName = "ScriptableObjects/BrainStates/BigDudeAttackState")]
public class BigDudeAttackState : MobState
{
    public override void Behavior()
    {
        switch (m_currentState)
        {
            case State.Phase1:
                if (user.PlayerWithinAttackRange())
                    user.MeleeAttack();
                break;
        }
    }
}
