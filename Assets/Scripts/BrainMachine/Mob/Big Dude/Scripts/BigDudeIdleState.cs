﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BigDudeIdleState", menuName = "ScriptableObjects/BrainStates/BigDudeIdleState")]
public class BigDudeIdleState : MobState
{
    public override void Behavior()
    {
        switch (m_currentState)
        {
            case State.Phase1:
                user.SearchForPlayerAndBullet();
                if (user.PlayerBulletFound())
                    m_currentState = State.Phase3;
                else if (user.PlayerFound())
                    m_currentState = State.Phase2;
                break;
            case State.Phase2:
                user.PushBrainState(1);
                break;
            case State.Phase3:
                //user.SearchForPlayer(user.AggroRange() * 10);
                user.PushBrainState(1);
                break;
        }
    }
}
