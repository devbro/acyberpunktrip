﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MobState : BrainState
{
    protected Mob user;
    public override void SetUser(BrainUser user)
    {
        this.user = user as Mob;
    }
}
