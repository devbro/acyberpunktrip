﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mob : BrainUser
{
    [Header("BATTLE")]
    [SerializeField]
    private LayerMask m_playerLayer;
    [SerializeField]
    private LayerMask m_playerBulletLayer;
    [SerializeField]
    private GameObject m_player;
    [SerializeField]
    private float m_aggroRange;
    [SerializeField]
    private float m_loseAggroRange;
    [SerializeField]
    private float m_attackDamage;
    [SerializeField]
    private float m_attackRadius;
    [SerializeField]
    private float m_attackRange;
    private Vector3 m_attackPosition;
    private Vector3 m_startPosition;
    private Vector3 m_newPosition;
    private Vector3 m_firstPlayerBulletPos;
    [SerializeField]
    private float m_dodgeAngle;
    [SerializeField]
    private float m_dodgeDistance;
    [SerializeField]
    private float m_withinPlayerPos;

    [SerializeField]
    private GameObject m_mobToSpawn;
    [SerializeField]
    private int m_numberOfMobsToSpawn;

    [Header("WEAPON")]
    [SerializeField]
    private GameObject m_weapon;
    private SpriteRenderer m_weaponSpriteRenderer;
    private EquippedWeapon m_equippedWeapon;
    [SerializeField]
    private GameObject m_Barrel;

    [SerializeField]
    private float m_timer;
    private float m_currentTimer;

    private MobMovement m_move;
    private List<Transform> m_flock = new List<Transform>();
    [SerializeField]
    private float m_attackPosOffset;
    private float m_randomAttackPosition;


    protected override void Awake()
    {
        base.Awake();
        m_currentTimer = m_timer;
        m_startPosition = transform.position;
        m_equippedWeapon = GetComponent<EquippedWeapon>();
        m_move = GetComponent<MobMovement>();
        m_randomAttackPosition = Random.Range(-m_attackPosOffset, m_attackPosOffset);
    }

    #region PRIVATE

    private Vector3 Move(Vector3 pos)
    {
        return Vector3.MoveTowards(m_move.Move(transform, m_flock), pos, m_moveSpeed * Time.deltaTime);
        //return Vector3.MoveTowards(transform.position, pos, m_moveSpeed * Time.deltaTime);
    }

    #endregion

    #region PUBLIC

    public void CreateFlock(List<Mob> mobs)
    {
        if (m_flock.Count > 0)
            m_flock.Clear();
        foreach (Mob mob in mobs)
            if (mob != this)
                m_flock.Add(mob.transform);
    }

    public Vector3 Position()
    {
        return transform.position;
    }

    public GameObject Player()
    {
        return m_player;
    }

    public Vector3 PlayerPosition()
    {
        return m_player.transform.position;
    }

    public float AggroRange()
    {
        return m_aggroRange;
    }

    public float AttackRange()
    {
        return m_attackRange;
    }

    public float LoseAggroRange()
    {
        return m_loseAggroRange;
    }

    public bool PlayerWithinAggroRange()
    {
        return Vector3.Distance(transform.position, m_player.transform.position) < m_aggroRange;
    }

    public bool PlayerWithinAttackRange()
    {
        return Vector3.Distance(transform.position, m_player.transform.position) < m_attackRange;
    }

    // ONLY NINJA
    public bool AttackPositionReached()
    {
        return Vector3.Distance(transform.position, m_attackPosition) < 0.01f;
    }

    // ONLY NINJA
    public void MoveToAttackPosition()
    {
        transform.position = Move(m_attackPosition);
    }

    public bool PlayerPositionReached()
    {
        return Vector3.Distance(transform.position, m_player.transform.position) < m_withinPlayerPos;
    }

    public void MoveToPlayerPosition()
    {
        // offset where to move based on spawnPos?
        transform.position = Move(new Vector2(m_player.transform.position.x, m_player.transform.position.y) * m_randomAttackPosition);
    }

    public void SetRandomAttackPosition()
    {
        m_randomAttackPosition = Random.Range(-m_attackPosOffset, m_attackPosOffset);
    }

    // ONLY NINJA
    public bool NewPositionReached()
    {
        return Vector3.Distance(transform.position, m_newPosition) < 0.01f;
    }

    // ONLY NINJA
    public void MoveToNewPosition()
    {
        transform.position = Move(m_newPosition);
    }

    public float DistanceToPlayer()
    {
        return Vector3.Distance(transform.position, m_player.transform.position);
    }

    public void SearchForPlayer()
    {
        Collider2D hit = Physics2D.OverlapCircle(transform.position, m_aggroRange, m_playerLayer);
        if (hit != null)
        {
            m_player = hit.gameObject;
            m_attackPosition = m_player.transform.position;
        }
    }

    // WHY DO I HAVE TO SEARCHFORPLAYER?
    public void SearchForPlayer(float range)
    {
        Collider2D hit = Physics2D.OverlapCircle(transform.position, range, m_playerLayer);
        if (hit != null)
        {
            m_player = hit.gameObject;
            m_attackPosition = m_player.transform.position;
        }
    }

    // ONLY NINJA
    public void SearchForPlayerPosition()
    {
        Collider2D hit = Physics2D.OverlapCircle(transform.position, m_aggroRange, m_playerLayer);
        if (hit != null)
        {
            m_attackPosition = m_player.transform.position;
        }
    }

    public void SearchForPlayerBullet()
    {
        Collider2D hit = Physics2D.OverlapCircle(transform.position, m_aggroRange, m_playerBulletLayer);
        if (hit != null)
        {
            m_firstPlayerBulletPos = hit.gameObject.transform.position;
            SearchForPlayer(m_aggroRange * 100);
        }
    }

    public void SearchForPlayerAndBullet()
    {
        SearchForPlayer();
        SearchForPlayerBullet();
    }

    public bool PlayerFound()
    {
        return m_player != null ? true : false;
    }

    public bool PlayerBulletFound()
    {
        return m_firstPlayerBulletPos != Vector3.zero ? true : false;
    }

    public void MeleeAttack()
    {
        Collider2D hit = Physics2D.OverlapCircle(transform.position, m_attackRadius, m_playerLayer);
        if (hit != null)
        {
            if (hit.GetComponent<IDamageable>() != null)
            {
                var target = hit.GetComponent<IDamageable>();
                target.ApplyDamage(m_attackDamage, gameObject.name);
            }
            else
                return;
        }
    }

    public void RangedAttack()
    {
        m_equippedWeapon.RangedAttack(m_Barrel.transform.position, m_weapon.transform.rotation, m_player);
        RotateEnemy();
    }

    public void NewPositionFromBullet()
    {
        // DISTANCE BETWEEN BLADE RUNNER AND PLAYER BULLET
        var distance = (m_firstPlayerBulletPos - transform.position).magnitude;
        // DIRECTION FOR BLADE RUNNER TO PLAYER BULLET
        var direction = (transform.position - m_firstPlayerBulletPos).normalized;
        // OFFSET DIRECTION BY X DEGREES
        var rotationMatrix = Quaternion.Euler(0, 0, m_dodgeAngle);
        // CALCULATE NEW POSITION BASED ON DIRECTION AND OFFSET
        var newDirection = rotationMatrix * direction;
        m_newPosition = newDirection * (distance / m_dodgeDistance);
    }

    public void NewPositionFromPlayer()
    {
        // DISTANCE BETWEEN BLADE RUNNER AND PLAYER BULLET
        var distance = (m_player.transform.position - transform.position).magnitude;
        // DIRECTION FOR BLADE RUNNER TO PLAYER BULLET
        var direction = (transform.position - m_player.transform.position).normalized;
        // OFFSET DIRECTION BY X DEGREES
        var rotationMatrix = Quaternion.Euler(0, 0, -m_dodgeAngle);
        // CALCULATE NEW POSITION BASED ON DIRECTION AND OFFSET
        var newDirection = rotationMatrix * direction;
        m_newPosition = newDirection * (distance / m_dodgeDistance);
    }

    public void SpawnMobs()
    {
        for (int i = 0; i < m_numberOfMobsToSpawn; i++)
        {
            var mob = Instantiate(m_mobToSpawn, transform.position + new Vector3(i, i, 0), Quaternion.identity);
        }
    }

    private void RotateEnemy()
    {
        FlipSprite(m_player.transform);
        WeaponLookAtTarget();
    }

    private void FlipSprite(Transform target)
    {
        Vector2 thisCharacter = transform.position;
        Vector2 targetPosition = target.position;

        if (CheckIfSpriteShouldFlip(targetPosition, thisCharacter))
            RotateGameObject(180, -1);
        else
            RotateGameObject(0, 1);
    }

    private void RotateGameObject(float rotation, float scale)
    {
        transform.localRotation = Quaternion.Euler(0, rotation, 0);
        m_weapon.transform.localScale = new Vector3(1, scale, 1);
    }

    private bool CheckIfSpriteShouldFlip(Vector2 firstPos, Vector2 secondPos)
    {
        return (firstPos.x < secondPos.x) ? true : false;
    }

    private void WeaponLookAtTarget()
    {
        var direction = m_player.transform.position - m_weapon.transform.position;
        var angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        m_weapon.transform.rotation = Quaternion.Slerp(m_weapon.transform.rotation, rotation, 10 * Time.deltaTime);
    }

    #endregion

    #region TIMER

    public void StartTimer()
    {
        m_currentTimer -= Time.deltaTime;
    }

    public void ResetTimer()
    {
        m_currentTimer = m_timer;
    }

    public bool TimerEnded()
    {
        return m_currentTimer <= 0 ? true : false;
    }

    #endregion
}
