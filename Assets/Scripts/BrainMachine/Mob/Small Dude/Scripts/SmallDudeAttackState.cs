﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SmallDudeAttackState", menuName = "ScriptableObjects/BrainStates/SmallDudeAttackState")]
public class SmallDudeAttackState : MobState
{
    public override void Behavior()
    {
        switch (m_currentState)
        {
            case State.Phase1:
                if (user.PlayerWithinAttackRange())
                {
                    user.MeleeAttack();
                    m_currentState = State.Phase2;
                }
                break;
            case State.Phase2:
                user.ApplyDamage(1000, this.name);
                break;
        }
    }
}
