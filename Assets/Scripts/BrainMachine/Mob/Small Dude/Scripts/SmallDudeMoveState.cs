﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SmallDudeMoveState", menuName = "ScriptableObjects/BrainStates/SmallDudeMoveState")]
public class SmallDudeMoveState : MobState
{
    public override void Behavior()
    {
        switch (m_currentState)
        {
            case State.Phase1:
                user.MoveToPlayerPosition();
                if (user.PlayerPositionReached())
                    m_currentState = State.Phase2;
                break;
            case State.Phase2:
                ResetStatePhase();
                user.PushBrainState(2);
                break;
        }
    }
}
