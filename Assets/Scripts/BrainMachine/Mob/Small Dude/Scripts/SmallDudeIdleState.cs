﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SmallDudeIdleState", menuName = "ScriptableObjects/BrainStates/SmallDudeIdleState")]
public class SmallDudeIdleState : MobState
{
    public override void Behavior()
    {
        switch (m_currentState)
        {
            case State.Phase1:
                user.SearchForPlayerAndBullet();
                if (user.PlayerFound())
                    m_currentState = State.Phase2;
                else if (user.PlayerBulletFound())
                    m_currentState = State.Phase3;
                break;
            case State.Phase2:
                user.PushBrainState(1);
                break;
            case State.Phase3:
                user.SearchForPlayer(user.AggroRange() * 10);
                user.PushBrainState(1);
                break;
        }
    }
}
