﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FirstBossIdleState", menuName = "ScriptableObjects/BrainStates/FirstBossIdleState")]
public class FirstBossIdleState : BossState
{
    public override void Behavior()
    {
        switch (m_currentState)
        {
            case State.Phase1:
                if (user.TargetWithinAggroRange())
                {
                    user.ActivateInterface();
                    user.ModuloAttack();
                    user.SetNextRandomPosition();
                    m_currentState = State.Phase2;
                }
                break;
            case State.Phase2:
                if (user.AttackDone())
                {
                    m_currentState = State.Phase3;
                }
                break;
            case State.Phase3:
                user.GoToNextPosition();
                if (user.NextPositionReached())
                {
                    user.NormalAttack();
                    user.SetNextRandomPosition();
                    m_currentState = State.Phase4;
                }
                break;
            case State.Phase4:
                if (user.AttackDone())
                {
                    ResetStatePhase();
                    user.PushBrainState(1);
                }
                break;
        }
    }
}
