﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FirstBossState3", menuName = "ScriptableObjects/BrainStates/FirstBossState3")]
public class FirstBossState3 : BossState
{
    public override void Behavior()
    {
        switch (m_currentState)
        {
            case State.Phase1:
                if (user.GetCurrentHealth() < 444)
                {
                    user.PopBrainState();
                }
                user.SingleAttack();
                user.PushBrainState(4);
                break;
            case State.Phase2:

                break;
        }
    }
}
