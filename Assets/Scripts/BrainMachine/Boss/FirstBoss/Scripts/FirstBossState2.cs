﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FirstBossState2", menuName = "ScriptableObjects/BrainStates/FirstBossState2")]
public class FirstBossState2 : BossState
{
    public override void Behavior()
    {
        switch (m_currentState)
        {
            case State.Phase1:
                if (user.AttackDone() && user.NextPositionReached())
                {
                    user.GoToNextPosition();
                    m_currentState = State.Phase2;
                }
                break;
            case State.Phase2:
                if (user.NextPositionReached())
                {
                    user.SetNextRandomPosition();
                    user.ModuloAttack();
                    ResetStatePhase();
                    user.PushBrainState(3);
                }
                break;
        }
    }
}
