﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FirstBossState1", menuName = "ScriptableObjects/BrainStates/FirstBossState1")]
public class FirstBossState1 : BossState
{
    public override void Behavior()
    {
        switch (m_currentState)
        {
            case State.Phase1:
                user.GoToNextPosition();
                if (user.NextPositionReached())
                {
                    user.NormalAttack();
                    user.SetNextRandomPosition();
                    m_currentState = State.Phase2;
                }
                break;
            case State.Phase2:
                //ResetStatePhase();
                //user.PushBrainState(2);
                break;
        }

    }
}
