﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FirstBossState4", menuName = "ScriptableObjects/BrainStates/FirstBossState4")]
public class FirstBossState4 : BossState
{
    public override void Behavior()
    {
        switch (m_currentState)
        {
            case State.Phase1:
                if (user.AttackDone())
                {
                    user.FollowTarget();
                    user.StartTimer();
                    m_currentState = State.Phase2;
                }
                break;
            case State.Phase2:
                if (user.TimerEnded())
                {
                    user.ResetTimer();
                    ResetStatePhase();
                    user.PopBrainState();
                }
                break;
        }
    }
}
