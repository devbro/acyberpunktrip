﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BossState : BrainState
{
    protected Boss user;
    public override void SetUser(BrainUser user)
    {
        this.user = user as Boss;
    }
}
