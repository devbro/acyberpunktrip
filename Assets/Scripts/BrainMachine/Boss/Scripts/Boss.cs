﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BulletHellSystem))]
public class Boss : BrainUser
{
    [Header("NAME")]
    [SerializeField]
    private string m_bossName;
    [Header("BULLET HELL SYSTEM")]
    [SerializeField]
    private string m_waveForm;
    [SerializeField]
    private BulletHellSystem m_bulletSystem;
    [SerializeField]
    private int m_bulletHellPoints;

    [Header("BATTLE STUFF")]
    [SerializeField]
    private GameObject m_target;
    [SerializeField]
    private float m_aggroRange;
    [SerializeField]
    private float m_timer;
    private float m_currentTimer;
    [SerializeField]
    private Transform[] m_bossPositions;
    private Vector3 m_startPosition;
    private Vector3 m_currentPosition;
    private Vector3 m_nextPosition;

    private BossInterface m_interface;


    protected override void Awake()
    {
        base.Awake();
        gameObject.name = m_bossName;
        m_bulletSystem = GetComponent<BulletHellSystem>();
        m_bulletSystem.CreateShootPoints(transform.position, m_bulletHellPoints);
        m_target = GameObject.FindGameObjectWithTag("Player");
        m_bulletSystem.SetTarget(m_target);
        m_interface = GetComponent<BossInterface>();
        m_startPosition = m_bossPositions[0].position;
        m_currentPosition = m_startPosition;
        m_currentTimer = m_timer;
    }

    #region PRIVATE
    private Vector3 ReturnRandomPosition()
    {
        var pos = Random.Range(0, m_bossPositions.Length - 1);
        var newPos = m_bossPositions[pos].position;
        if (newPos != m_currentPosition)
        {
            return m_currentPosition = newPos;
        }
        else
            return ReturnRandomPosition();
    }
    #endregion

    #region PUBLIC
    public string Name()
    {
        return m_bossName;
    }

    public Vector3 Position()
    {
        return transform.position;
    }

    public GameObject Target()
    {
        return m_target;
    }

    public Vector3 TargetPosition()
    {
        return m_target.transform.position;
    }

    public float AggroRange()
    {
        return m_aggroRange;
    }

    public bool TargetWithinAggroRange()
    {
        return Vector3.Distance(transform.position, m_target.transform.position) < m_aggroRange;
    }

    public void ActivateInterface()
    {
        m_interface.SetInterface(true);
    }

    public void GoToNextPosition()
    {
        if (Vector3.Distance(transform.position, m_nextPosition) > 0.1)
        {
            transform.position = Vector3.MoveTowards(transform.position, m_nextPosition, m_moveSpeed * Time.deltaTime);
        }
    }

    public void SetNextRandomPosition()
    {
        m_nextPosition = ReturnRandomPosition();
    }

    public bool NextPositionReached()
    {
        return Vector3.Distance(transform.position, m_nextPosition) < 0.1;
    }

    public void FollowTarget()
    {
        transform.position = Vector3.MoveTowards(transform.position, m_target.transform.position, m_moveSpeed * Time.deltaTime);
    }

    public void ChargeTarget()
    {
        // SAME AS FOLLOW?
    }

    public void StartTimer()
    {
        m_currentTimer -= Time.deltaTime;
    }

    public void ResetTimer()
    {
        m_currentTimer = m_timer;
    }

    public bool TimerEnded()
    {
        if (m_currentTimer <= 0)
            return true;
        return false;
    }

    #region ATTACKS

    public void NormalAttack()
    {
        m_bulletSystem.BulletHell("normal");
    }

    public void ModuloAttack()
    {
        m_bulletSystem.BulletHell("modulo");
    }

    public void SingleAttack()
    {
        m_bulletSystem.BulletHell("single");
    }

    public void LineAttack()
    {
        m_bulletSystem.BulletHell("line");
    }

    public void ArcAttack()
    {
        m_bulletSystem.BulletHell("arc");
    }

    public bool AttackDone()
    {
        return !m_bulletSystem.BulletHellRunning();
    }

    #endregion

    #endregion
}
