﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BrainMachine))]
public abstract class BrainUser : Entity
{
    private BrainMachine BRAIN;

    protected override void Awake()
    {
        base.Awake();
        BRAIN = GetComponent<BrainMachine>();
    }

    private void Start()
    {
        BRAIN.SetUser(this);
    }

    public void PushBrainState(int num)
    {
        BRAIN.PushBrainState(BRAIN.State(num));
    }

    public void PopBrainState()
    {
        BRAIN.PopBrainState();
    }
}
