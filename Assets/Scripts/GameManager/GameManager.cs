﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Header("Special Abilities Time Scale")]
    [SerializeField]
    private float deadEyeTimeScale;

    [Header("References")]
    [SerializeField]
    private Player player;

    [SerializeField]
    private TextMeshProUGUI killedText;

    #region DONTUSE
    //public List<Entity> m_entities = new List<Entity>();
    //public List<EquippedWeapon> m_allEquippedWeapons = new List<EquippedWeapon>();
    //public HashSet<Projectile> m_projectiles = new HashSet<Projectile>();
    #endregion

    private void Awake()
    {
        #region DONTUSE
        //player.DeadEyeStartedAction += SlowEntity;
        //player.DeadEyeStartedAction += FindAllProjectiles;
        //player.DeadEyeStartedAction += SlowNewProjectiles;
        //player.DeadEyeExecutedAction += SetEntityNormalSpeed;
        //player.DeadEyeExecutedAction += SetProjectileNormalSpeed;
        //player.DeadEyeExecutedAction += SetNewProjectilesNormalSpeed;
        //var entities = FindObjectsOfType<Entity>();
        //m_entities.AddRange(entities);
        //FindAllEquippedWeaponsInScene();
        #endregion

        killedText.gameObject.SetActive(false);
        FindPlayer();
    }

    private void FindPlayer()
    {
        var p = GameObject.FindGameObjectWithTag("Player");
        player = p.GetComponent<Player>();
        if (player != null)
        {
            player.DeadEyeStartedAction += SetDeadEyeMotion;
            player.DeadEyeExecutedAction += SetNormalTime;
            player.EntityIsDead += ShowKiller;
        }
    }

    private void ShowKiller()
    {
        killedText.text += player.GetKiller().ToUpper();
        killedText.gameObject.SetActive(true);
    }

    private void SetDeadEyeMotion()
    {
        Time.timeScale = deadEyeTimeScale;
    }

    private void SetNormalTime()
    {
        Time.timeScale = 1;
    }


    #region DONTUSE
    // TO-DO: FIND ALL NEWLY SPAWNED BULLETS SO I CAN GIVE THEM THEIR ORIGINAL SPEED AS WELL.

    //private void Update()
    //{
    //    //RemoveDisabledEntitiesFromList();
    //}

    //private void RemoveDisabledEntitiesFromList()
    //{
    //    foreach (Entity entity in m_entities)
    //    {
    //        if (!entity.enabled)
    //            m_entities.Remove(entity);
    //    }
    //}

    //private void FindAllEquippedWeaponsInScene()
    //{
    //    foreach (Entity entity in m_entities)
    //    {
    //        var equippedWpn = entity.gameObject.GetComponent<EquippedWeapon>();
    //        if (equippedWpn != null)
    //        {
    //            m_allEquippedWeapons.Add(equippedWpn);
    //        }
    //    }
    //}

    //private void SlowNewProjectiles()
    //{
    //    foreach (EquippedWeapon eW in m_allEquippedWeapons)
    //    {
    //        eW.m_rangedWeapon.SetProjectileSpeed(0.3f);
    //    }
    //}

    //private void SetNewProjectilesNormalSpeed()
    //{
    //    foreach (EquippedWeapon eW in m_allEquippedWeapons)
    //    {
    //        eW.m_rangedWeapon.SetProjectileSpeed(eW.m_rangedWeapon.GetOriginalProjectileSpeed());
    //    }
    //}

    //private void SlowEntity()
    //{
    //    Debug.Log("Dead eye activated");
    //    foreach (Entity entity in m_entities)
    //    {
    //        var movable = entity.GetComponent<IMovable>();
    //        if (movable != null)
    //            movable.SetMoveSpeed(1);
    //    }
    //}

    //private void FindAllProjectiles()
    //{
    //    var projectile = FindObjectsOfType<Projectile>();
    //    foreach (Projectile proj in projectile)
    //    {
    //        m_projectiles.Add(proj);
    //    }
    //    SlowProjectiles();
    //}

    //private void SlowProjectiles()
    //{
    //    foreach (Projectile projectile in m_projectiles)
    //    {
    //        if (projectile == null)
    //            return;
    //        var movable = projectile.gameObject.GetComponent<IMovable>();
    //        if (movable != null)
    //            movable.SetMoveSpeed(0.3f);
    //    }
    //}

    //private void SetProjectileNormalSpeed()
    //{
    //    foreach (Projectile projectile in m_projectiles)
    //    {
    //        if (projectile == null)
    //            return;
    //        var movable = projectile.gameObject.GetComponent<IMovable>();
    //        if (movable != null)
    //        {
    //            var owner = projectile.GetOwner();
    //            var o = owner.GetComponent<EquippedWeapon>();
    //            if (o != null)
    //            {
    //                movable.SetMoveSpeed(o.m_rangedWeapon.GetOriginalProjectileSpeed());
    //            }
    //        }
    //    }
    //}

    //private void SetEntityNormalSpeed()
    //{
    //    foreach (Entity entity in m_entities)
    //    {
    //        var movable = entity.GetComponent<IMovable>();
    //        if (movable != null)
    //            movable.SetMoveSpeed(entity.GetOriginalMoveSpeed());
    //    }
    //}
    #endregion

}
