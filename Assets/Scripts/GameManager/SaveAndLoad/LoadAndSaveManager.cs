﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;

public class LoadAndSaveManager : MonoBehaviour
{
    [SerializeField]
    private Player Player;

    private string path = @"WHERE_TO_SAVE";

    private void Awake()
    {
        // Probably move this.
        DontDestroyOnLoad(this);
    }

    public void Save()
    {
        Debug.Log("Save");
        PlayerData save = Player.Save();

        CreateXmlDocument("Player1", Player.GetHealth(), "AK", save.position.x.ToString(), save.position.y.ToString());
    }

    public void Load()
    {
        Debug.Log("Load");

        XmlDocument load = new XmlDocument();
        load.Load(path + "\\SaveFile.xml");

        string positionX = "";
        string positionY = "";
        XmlNodeList xnList = load.SelectNodes("/SaveFile/PlayerStats");
        foreach (XmlNode xn in xnList)
        {
            positionX = xn["PlayerXPosition"].InnerText;
            positionY = xn["PlayerYPosition"].InnerText;
        }
        float x = float.Parse(positionX);
        float y = float.Parse(positionY);

        Player.Load(new Vector2(x, y));
    }

    private void CreateXmlDocument(string pName, float pHealth, string pWeapon, string pXPosition, string pYPosition)
    {
        //FROM:
        //https://stackoverflow.com/questions/11492705/how-to-create-an-xml-document-using-xmldocument

        XmlDocument doc = new XmlDocument();

        //(1) the xml declaration is recommended, but not mandatory
        XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
        XmlElement root = doc.DocumentElement;
        doc.InsertBefore(xmlDeclaration, root);

        //(2) string.Empty makes cleaner code
        XmlElement documentName = doc.CreateElement(string.Empty, "SaveFile", string.Empty);
        doc.AppendChild(documentName);

        //PlayerData
        XmlElement playerStats = doc.CreateElement(string.Empty, "PlayerStats", string.Empty);
        documentName.AppendChild(playerStats);

        XmlElement nameElement = doc.CreateElement(string.Empty, "PlayerHealth", string.Empty);
        XmlText name = doc.CreateTextNode(pName);
        playerStats.AppendChild(nameElement);
        nameElement.AppendChild(name);

        XmlElement healthElement = doc.CreateElement(string.Empty, "PlayerHealth", string.Empty);
        XmlText health = doc.CreateTextNode(pHealth.ToString());
        playerStats.AppendChild(healthElement);
        healthElement.AppendChild(health);

        XmlElement weaponElement = doc.CreateElement(string.Empty, "PlayerWeapon", string.Empty);
        XmlText weapon = doc.CreateTextNode(pWeapon);
        playerStats.AppendChild(weaponElement);
        weaponElement.AppendChild(weapon);

        XmlElement xPosElement = doc.CreateElement(string.Empty, "PlayerXPosition", string.Empty);
        XmlText xPos = doc.CreateTextNode(pXPosition);
        playerStats.AppendChild(xPosElement);
        xPosElement.AppendChild(xPos);

        XmlElement yPosElement = doc.CreateElement(string.Empty, "PlayerYPosition", string.Empty);
        XmlText yPos = doc.CreateTextNode(pYPosition);
        playerStats.AppendChild(yPosElement);
        yPosElement.AppendChild(yPos);

        doc.Save(path + "\\SaveFile.xml");
    }
}
