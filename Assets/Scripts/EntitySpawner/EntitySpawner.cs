﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntitySpawner : MonoBehaviour
{
    [SerializeField]
    private int m_numberOfEntities;
    [SerializeField]
    private float m_triggerSpawnRange;
    [SerializeField]
    private int m_spawnRange;
    [SerializeField]
    private LayerMask m_playerLayer;
    private bool m_haveSpawned = false;
    private List<Vector2> m_spawnPositions = new List<Vector2>();
    private List<Mob> m_mobs = new List<Mob>();
    private ObjectPool m_objectPool;

    [SerializeField]
    private GameObject m_player;

    private void Start()
    {
        m_objectPool = GetComponent<ObjectPool>();
        for (int i = 0; i < m_numberOfEntities; i++)
            GenerateSpawnPosition();
    }

    private void Update()
    {
        SpawnEntities();
        CheckPlayerDistance();
    }

    private void CreateMobFlock()
    {
        foreach (Mob mob in m_mobs)
            mob.CreateFlock(m_mobs);
    }

    private void SpawnEntities()
    {
        if (!m_haveSpawned)
        {
            Collider2D hit = Physics2D.OverlapCircle(transform.position, m_triggerSpawnRange, m_playerLayer);
            if (hit != null)
            {
                m_player = hit.gameObject;
                for (int i = 0; i < m_spawnPositions.Count; i++)
                {
                    var entity = m_objectPool.GetObjectFromDictionary(m_objectPool.GetFromObjectArray(), 
                        new Vector3(m_spawnPositions[i].x, m_spawnPositions[i].y), Quaternion.identity);
                    var mob = entity.GetComponent<Mob>();
                    m_mobs.Add(mob);
                }
                CreateMobFlock();
                m_spawnPositions.Clear();
                m_haveSpawned = true;
            }
        }
    }

    private void GenerateSpawnPosition()
    {
        Vector2 tryPosition = new Vector2(transform.position.x, transform.position.y)
            + new Vector2(Random.Range(-m_spawnRange, m_spawnRange), Random.Range(-m_spawnRange, m_spawnRange));

        if (m_spawnPositions.Count == 0)
            m_spawnPositions.Add(tryPosition);
        else
        {
            foreach (Vector2 pos in m_spawnPositions)
            {
                if (tryPosition == pos)
                {
                    GenerateSpawnPosition();
                    return;
                }
            }
            m_spawnPositions.Add(tryPosition);
        }
    }

    private void CheckPlayerDistance()
    {
        if (m_player != null)
        {
            var dist = Vector3.Distance(transform.position, m_player.transform.position);
            int distToInt = (int)dist;
            if (distToInt > 20)
                m_haveSpawned = false;
        }
    }
}
