﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : Entity
{
    #region Variables

    [Header("STATS")]
    private PlayerControls m_controls;
    private EquippedWeapon m_equippedWeapon;
    private SpecialAbilities m_specialAbilities;
    private Rigidbody2D m_rigidBody;
    private BoxCollider2D m_collider;

    [Header("Animation")]
    [SerializeField]
    private Animator m_animator;

    private Transform m_transform;
    private Vector2 m_move;

    [Header("GameObjects")]
    [SerializeField]
    private GameObject m_rangedWeapon;
    [SerializeField]
    private GameObject m_meleeWeapon;
    private SpriteRenderer m_rangedWeaponSpriteRenderer;
    private SpriteRenderer m_meleeWeaponSpriteRenderer;
    private SpriteRenderer m_activeWeaponSpriteRenderer;
    [SerializeField]
    private GameObject m_rangedWeaponAttackPoint;
    [SerializeField]
    private GameObject m_meleeWeaponAttackPoint;
    private GameObject m_activeWeapon;
    private GameObject[] m_arrayOfWeapons;

    [Header("Particles")]
    [SerializeField]
    private ParticleSystem m_dustParticles;

    [Header("Crosshair")]
    [SerializeField]
    private GameObject Crosshair;
    [SerializeField]
    private float m_crosshairDistance;
    private Vector2 m_crosshairVector;
    private GameObject m_crosshairObject;
    private SpriteRenderer m_crosshairSpriteRenderer;
    private Vector2 m_aim;
    private float rangerAttackTrigger;
    private float meleeAttackTrigger;

    [SerializeField]
    private LayerMask m_enemyLayer;

    private Crosshair m_crosshair;
    private List<Collider2D> deadEyeTargets = new List<Collider2D>();

    [Header("Movement")]
    private bool m_canMove = true;
    [SerializeField]
    private float m_dashDistance;
    [SerializeField]
    private float m_dashSpeed;
    private bool m_canDash = true;
    private IEnumerator m_performDash;
    private IEnumerator m_performDeadEye;
    private IEnumerator m_performSwordDance;


    private GameObject m_target;

    #endregion

    #region Actions

    public event Action DeadEyeStartedAction;
    public event Action DeadEyeExecutedAction;

    #endregion

    #region GamePlay

    protected override void Awake()
    {
        base.Awake();
        m_controls = new PlayerControls();
        m_equippedWeapon = GetComponent<EquippedWeapon>();
        m_specialAbilities = GetComponent<SpecialAbilities>();

        m_rigidBody = GetComponent<Rigidbody2D>();
        m_collider = GetComponent<BoxCollider2D>();
        m_transform = gameObject.transform;

        // WORKED BEFORE - MOVED TO START
        //m_rangedWeaponSpriteRenderer = m_rangedWeapon.GetComponent<SpriteRenderer>();
        //m_rangedWeaponSpriteRenderer.sprite = m_equippedWeapon.EquippedRangedWeapon.sprite;
        //m_meleeWeaponSpriteRenderer = m_meleeWeapon.GetComponent<SpriteRenderer>();
        //m_meleeWeaponSpriteRenderer.sprite = m_equippedWeapon.EquippedMeleeWeapon.sprite;
        //CreateWeaponArray();

        m_crosshairVector = new Vector2(m_crosshairDistance, 0);
        m_crosshairObject = Instantiate(Crosshair, m_crosshairVector, Quaternion.identity);
        m_crosshairSpriteRenderer = m_crosshairObject.GetComponent<SpriteRenderer>();
        m_crosshair = m_crosshairObject.GetComponent<Crosshair>();

        // CONTROLS
        m_controls.mouseAndKey.RangedAttack.performed += ctx => rangerAttackTrigger = ctx.ReadValue<float>();
        m_controls.mouseAndKey.RangedAttack.canceled += ctx => rangerAttackTrigger = 0;
        m_controls.mouseAndKey.MeleeAttack.performed += ctx => meleeAttackTrigger = ctx.ReadValue<float>();
        m_controls.mouseAndKey.MeleeAttack.canceled += ctx => meleeAttackTrigger = 0;
        m_controls.mouseAndKey.Dash.performed += ctx => Dash();
        m_controls.mouseAndKey.StartDeadEye.performed += ctx => StartDeadEye();
        m_controls.mouseAndKey.ExecuteDeadEye.performed += ctx => ExecuteDeadEye();
        m_controls.mouseAndKey.MarkEnemyForDeadEye.performed += ctx => MarkEnemyForDeadEye();
        m_controls.mouseAndKey.ChangeWeapon.performed += ctx => ChangeWeapon();
        m_controls.mouseAndKey.ExecuteSwordDance.performed += ctx => StartSwordDance();
        m_controls.mouseAndKey.Move.performed += ctx => m_move = ctx.ReadValue<Vector2>();
        m_controls.mouseAndKey.Move.canceled += ctx => m_move = Vector2.zero;
        m_controls.mouseAndKey.Aim.performed += ctx => m_aim = ctx.ReadValue<Vector2>();
        m_controls.mouseAndKey.Aim.canceled += ctx => m_aim = Vector2.zero;
        //Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;

        m_target = m_crosshairObject;
    }

    private void Start()
    {
        m_controls.mouseAndKey.ExecuteDeadEye.Disable();
        m_controls.mouseAndKey.MarkEnemyForDeadEye.Disable();
        m_controls.mouseAndKey.MeleeAttack.Disable();
        m_controls.mouseAndKey.ExecuteSwordDance.Disable();

        m_rangedWeaponSpriteRenderer = m_rangedWeapon.GetComponent<SpriteRenderer>();
        m_rangedWeaponSpriteRenderer.sprite = m_equippedWeapon.EquippedRangedWeapon.sprite;
        m_meleeWeaponSpriteRenderer = m_meleeWeapon.GetComponent<SpriteRenderer>();
        m_meleeWeaponSpriteRenderer.sprite = m_equippedWeapon.EquippedMeleeWeapon.sprite;
        CreateWeaponArray();
    }

    private void Update()
    {
        PlayerInput();
    }

    public void PlayerInput()
    {
        Move();
        Aim();
        FlipSprite();
        WeaponLookAtTarget();
        RangedAttack();
        MeleeAttack();
        //ControllerAim();
        //CircularAim();
    }

    #region Weapon Stuff
    private void ChangeWeapon()
    {
        if (m_activeWeapon == m_arrayOfWeapons[0])
        {
            m_activeWeapon = m_arrayOfWeapons[1];
            m_activeWeaponSpriteRenderer = m_meleeWeaponSpriteRenderer;
            m_arrayOfWeapons[1].SetActive(true);
            m_arrayOfWeapons[0].SetActive(false);
            m_controls.mouseAndKey.RangedAttack.Disable();
            m_controls.mouseAndKey.StartDeadEye.Disable();
            m_controls.mouseAndKey.MeleeAttack.Enable();
            m_controls.mouseAndKey.ExecuteSwordDance.Enable();
        }
        else if (m_activeWeapon == m_arrayOfWeapons[1])
        {
            m_activeWeapon = m_arrayOfWeapons[0];
            m_activeWeaponSpriteRenderer = m_rangedWeaponSpriteRenderer;
            m_arrayOfWeapons[0].SetActive(true);
            m_arrayOfWeapons[1].SetActive(false);
            m_controls.mouseAndKey.MeleeAttack.Disable();
            m_controls.mouseAndKey.ExecuteSwordDance.Disable();
            m_controls.mouseAndKey.RangedAttack.Enable();
            m_controls.mouseAndKey.StartDeadEye.Enable();
        }
    }

    private void CreateWeaponArray()
    {
        m_arrayOfWeapons = new GameObject[2];
        m_arrayOfWeapons[0] = m_rangedWeapon;
        m_arrayOfWeapons[1] = m_meleeWeapon;
        m_activeWeapon = m_arrayOfWeapons[0];
        m_activeWeaponSpriteRenderer = m_rangedWeaponSpriteRenderer;
        m_arrayOfWeapons[1].SetActive(false);
    }

    private void RangedAttack(bool isDeadEye = false)
    {
        if (rangerAttackTrigger != 0 || isDeadEye)
            m_equippedWeapon.RangedAttack(m_rangedWeaponAttackPoint.transform.position, m_rangedWeapon.transform.rotation, m_target);
    }

    private void MeleeAttack()
    {
        if (meleeAttackTrigger != 0)
            m_equippedWeapon.MeleeAttack(FindMeleeTargets());
    }

    private Collider2D[] FindMeleeTargets()
    {
        Collider2D[] targets = Physics2D.OverlapCircleAll(m_meleeWeaponAttackPoint.transform.position, m_equippedWeapon.EquippedMeleeWeapon.attackRadius, m_enemyLayer);
        return targets;
    }

    private void ControllerAim()
    {
        // FOR CONTROLLER
        Vector2 position = new Vector2(m_aim.x, m_aim.y) * m_crosshairDistance;
        Vector2 playerPos = m_character.transform.position;

        if (Mathf.Abs(m_aim.x) != 0 || Mathf.Abs(m_aim.y) != 0)
        {
            m_crosshairObject.transform.position = position + playerPos; // Changed from localPosition to position.
            m_crosshairSpriteRenderer.enabled = true;
        }
        else
        {
            m_crosshairObject.transform.position = new Vector2(m_transform.position.x, m_transform.position.y) + m_crosshairVector;
            m_crosshairSpriteRenderer.enabled = false;
        }
    }

    private void CircularAim()
    {
        // GOES IN A CIRCLE AROUND THE PLAYER
        var pos = m_controls.mouseAndKey.Aim.ReadValue<Vector2>();
        var mousePosition = Camera.main.ScreenToWorldPoint(new Vector3(pos.x, pos.y, Camera.main.nearClipPlane));
        var playerPos = transform.position;
        var dir = mousePosition - playerPos;
        var dirNor = dir.normalized;
        var angle = Mathf.Atan2(dirNor.y, dirNor.x) * Mathf.Rad2Deg;
        var rot = Quaternion.AngleAxis(angle, Vector3.forward) * (Vector3.right * m_crosshairDistance);
        m_crosshairObject.transform.position = playerPos + rot;
    }

    private void Aim()
    {
        // CROSSHAIR FOLLOWS THE CURSOR
        var pos = m_controls.mouseAndKey.Aim.ReadValue<Vector2>();
        var mousePosition = Camera.main.ScreenToWorldPoint(new Vector3(pos.x, pos.y, Camera.main.nearClipPlane));
        m_crosshairObject.transform.position = mousePosition;
    }
    #endregion

    #region DeadEye
    private void StartDeadEye()
    {
        DeadEyeStartedAction?.Invoke();
        m_controls.mouseAndKey.StartDeadEye.Disable();
        m_controls.mouseAndKey.RangedAttack.Disable();
        m_controls.mouseAndKey.ChangeWeapon.Disable();
        m_controls.mouseAndKey.MarkEnemyForDeadEye.Enable();
        m_controls.mouseAndKey.ExecuteDeadEye.Enable();
        m_equippedWeapon.EquippedRangedWeapon = m_specialAbilities.DeadEye;
    }

    private void ExecuteDeadEye()
    {
        m_controls.mouseAndKey.ExecuteDeadEye.Disable();
        m_controls.mouseAndKey.MarkEnemyForDeadEye.Disable();

        if (m_performDeadEye != null)
            StopCoroutine(m_performDeadEye);
        m_performDeadEye = PerformDeadEyeShooting();
        StartCoroutine(m_performDeadEye);
    }

    private IEnumerator PerformDeadEyeShooting()
    {
        DeadEyeExecutedAction?.Invoke();
        m_controls.mouseAndKey.Aim.Disable();
        m_controls.mouseAndKey.Dash.Disable();
        foreach (Collider2D target in deadEyeTargets)
        {
            m_target = target.gameObject;
            RangedAttack(true);
            yield return new WaitForSecondsRealtime(m_specialAbilities.DeadEyeSpeed);
        }
        deadEyeTargets.Clear();
        m_target = m_crosshairObject;
        m_controls.mouseAndKey.RangedAttack.Enable();
        m_controls.mouseAndKey.StartDeadEye.Enable();
        m_controls.mouseAndKey.Aim.Enable();
        m_controls.mouseAndKey.Dash.Enable();
        m_controls.mouseAndKey.ChangeWeapon.Enable();
        m_equippedWeapon.EquippedRangedWeapon = m_equippedWeapon.GetOriginalRangedWeapon();
    }

    private void MarkEnemyForDeadEye()
    {
        var target = m_crosshair.FindTarget();
        if (target != null)
            deadEyeTargets.Add(target);
        else
            return;
    }
    #endregion

    #region SwordDance
    struct TransformAndPosition
    {
        public Transform transform;
        public Vector3 position;

        public TransformAndPosition(Transform trans, Vector3 pos)
        {
            transform = trans;
            position = pos;
        }

        public bool HasValue()
        {
            if (transform != null && position != null)
                return true;
            else
                return false;
        }
    }

    private void StartSwordDance()
    {
        m_controls.mouseAndKey.Move.Disable();
        m_controls.mouseAndKey.Aim.Disable();
        m_controls.mouseAndKey.Dash.Disable();
        m_controls.mouseAndKey.MeleeAttack.Disable();
        m_equippedWeapon.EquippedMeleeWeapon = m_specialAbilities.SwordDance;
        var playerPos = m_transform.position;
        var player = new TransformAndPosition(m_transform, playerPos);
        Stack<TransformAndPosition> targetStack = new Stack<TransformAndPosition>();
        targetStack.Push(player);
        List<Collider2D> colliderArray = new List<Collider2D>(FindMeleeTargets());
        if (colliderArray.Count > 0)
        {
            PushEnemiesToStack(playerPos, GetTransformAndPosition(colliderArray), targetStack);
            PopEnemiesAndAttack(targetStack);
        }
        m_controls.mouseAndKey.Move.Enable();
        m_controls.mouseAndKey.Aim.Enable();
        m_controls.mouseAndKey.Dash.Enable();
        m_controls.mouseAndKey.MeleeAttack.Enable();
    }

    private void ExecuteSwordDance(TransformAndPosition target, Stack<TransformAndPosition> targetStack)
    {
        if (m_performSwordDance != null)
            StopCoroutine(m_performSwordDance);
        m_performSwordDance = SwordDanceAttack(target, targetStack);
        StartCoroutine(m_performSwordDance);
    }

    private IEnumerator SwordDanceAttack(TransformAndPosition target, Stack<TransformAndPosition> targetStack)
    {
        float elapsedTime = 0f;
        while(elapsedTime <= m_specialAbilities.SwordDanceSpeed)
        {
            float prec = elapsedTime / m_specialAbilities.SwordDanceSpeed;
            m_transform.position = Vector3.MoveTowards(m_transform.position, target.position, prec);
            if (Vector3.Distance(m_transform.position, target.position) < 0.001f)
            {
                if (target.transform != m_transform)
                {
                    var damage = target.transform.GetComponent<IDamageable>();
                    damage.ApplyDamage(m_specialAbilities.SwordDance.damage, gameObject.name);
                }
                PopEnemiesAndAttack(targetStack);
                break;
            }
            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }

    private void PopEnemiesAndAttack(Stack<TransformAndPosition> targetStack)
    {
        if (targetStack.Count > 0)
        {
            TransformAndPosition target = targetStack.Pop();
            ExecuteSwordDance(target, targetStack);
        }
        else
            return;
    }

    private void PushEnemiesToStack(Vector3 playerPos, List<TransformAndPosition> enemyList, Stack<TransformAndPosition> targetStack)
    {
        if (enemyList.Count > 0)
        {
            float longestDistance = 0;
            TransformAndPosition enemy = new TransformAndPosition();
            foreach (TransformAndPosition target in enemyList)
            {
                var targetTransform = target.transform;
                var distance = Vector3.Distance(playerPos, target.position);
                if (distance > longestDistance)
                {
                    longestDistance = distance;
                    enemy = target;
                }
            }
            if (enemy.HasValue())
            {
                enemyList.Remove(enemy);
                targetStack.Push(enemy);
                PushEnemiesToStack(playerPos, enemyList, targetStack);
            }
        }
        else
            return;
    }

    private List<TransformAndPosition> GetTransformAndPosition(List<Collider2D> list)
    {
        List<TransformAndPosition> colliderToTransform = new List<TransformAndPosition>();
        foreach (Collider2D col in list)
        {
            var gameObject = col.gameObject;
            var trans = gameObject.transform;
            var position = trans.position;
            var transAndPos = new TransformAndPosition(trans, position);
            colliderToTransform.Add(transAndPos);
        }
        return colliderToTransform;
    }
    #endregion

    #region Movement
    private void Move()
    {
        if (m_canMove)
        {
            Vector2 m = new Vector2(m_move.x, m_move.y) * Time.deltaTime * m_moveSpeed;
            m_transform.Translate(m, Space.World);
        }
    }

    private void Dash()
    {
        Vector3 direction = new Vector2(m_move.x, m_move.y);
        if (direction != Vector3.zero && m_canDash)
        {
            //m_crosshairObject.SetActive(false);
            CanDashAndMove(false);
            Vector3 newPosition = m_transform.position + (direction * m_dashDistance);
            if (m_performDash != null)
                StopCoroutine(m_performDash);
            m_performDash = DashCharacter(newPosition);
            CreateDust();
            m_collider.enabled = false;
            StartCoroutine(m_performDash);
        }
        else
            return;
    }

    private IEnumerator DashCharacter(Vector3 pos)
    {
        float elapsedTime = 0;
        while (elapsedTime <= m_dashSpeed)
        {
            float prec = elapsedTime / m_dashSpeed;
            m_transform.position = Vector3.MoveTowards(m_transform.position, pos, prec);
            if (m_transform.position - pos == Vector3.zero)
            {
                CanDashAndMove(true);
                m_collider.enabled = true;
                //m_crosshairObject.SetActive(true);
                break;
            }
            elapsedTime += Time.deltaTime;
            yield return null; // If not the loop will execute in one frame.
        }
    }

    private void CanDashAndMove(bool set)
    {
        m_canDash = set;
        m_canMove = set;
    }
    #endregion

    private void FlipSprite()
    {
        Vector2 thisCharacter = m_transform.position;
        Vector2 crosshairPosition = m_target.transform.position;

        if (CheckIfSpriteShouldFlip(crosshairPosition, thisCharacter))
            RotateGameObject(180, -1);
        else
            RotateGameObject(0, 1);
    }

    private void RotateGameObject(float rotation, float scale)
    {
        m_transform.localRotation = Quaternion.Euler(0, rotation, 0);
        m_activeWeapon.transform.localScale = new Vector3(1, scale, 1);
    }

    private bool CheckIfSpriteShouldFlip(Vector2 firstPos, Vector2 secondPos)
    {
        return (firstPos.x < secondPos.x) ? true : false;
    }

    private void WeaponLookAtTarget()
    {
        var direction = m_target.transform.position - m_activeWeapon.transform.position;
        var angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        m_activeWeapon.transform.rotation = Quaternion.Slerp(m_activeWeapon.transform.rotation, rotation, Time.deltaTime * 10);
    }

    private void CreateDust()
    {
        m_dustParticles.Play();
    }

    #endregion

    #region OnEnable/Disable
    private void OnEnable()
    {
        m_controls.mouseAndKey.Enable();
    }

    private void OnDisable()
    {
        m_controls.mouseAndKey.Disable();
    }
    #endregion

    #region SaveAndLoad
    public PlayerData Save()
    {
        PlayerData data = new PlayerData(this.gameObject.transform.position);
        return data;
    }

    public void Load(Vector2 position)
    {
        this.gameObject.transform.position = position;
    }

    public float GetHealth()
    {
        return _health;
    }
    #endregion
}
