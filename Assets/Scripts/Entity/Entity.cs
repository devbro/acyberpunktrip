﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Entity : MonoBehaviour, IDamageable, IMovable
{
    #region Variables
    [Header("STATS")]
    [SerializeField]
    protected float _health;
    private float m_currentHealth;
    [SerializeField]
    protected float m_moveSpeed;
    private float m_originalMoveSpeed;


    private List<string> m_killer = new List<string>();

    [Header("CHARACTER")]
    [SerializeField]
    protected GameObject m_character;

    [Header("SPRITES")]
    [SerializeField]
    protected Sprite m_characterSprite;

    public event Action UpdateCurrentHealth;
    public event Action EntityIsDead;

    #endregion

    #region Methods

    protected virtual void Awake()
    {
        m_character.GetComponent<SpriteRenderer>().sprite = m_characterSprite;
        m_originalMoveSpeed = m_moveSpeed;
        m_currentHealth = _health;
    }

    public float GetOriginalMoveSpeed()
    {
        return m_originalMoveSpeed;
    }

    public float GetCurrentHealth()
    {
        return m_currentHealth;
    }

    public string GetKiller()
    {
        return m_killer[0];
    }

    #endregion

    #region Interfaces

    public void ApplyDamage(float damage, string owner)
    {
        m_currentHealth -= damage;
        UpdateCurrentHealth?.Invoke();

        if (m_currentHealth <= 0)
        {
            // OBJECT POOL STUFF
            m_killer.Add(owner);
            EntityIsDead?.Invoke();
            m_killer.Clear();
            gameObject.SetActive(false);
        }
    }

    public void SetMoveSpeed(float movespeed)
    {
        m_moveSpeed = movespeed;
    }

    #endregion
}
