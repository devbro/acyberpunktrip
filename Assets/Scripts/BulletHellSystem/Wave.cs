﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Wave", menuName = "ScriptableObjects/Wave")]
public class Wave : ScriptableObject
{
    public int m_counter;
    public float m_delay;
    public GameObject m_projectile;
    public float m_bulletDamage;
    public float m_bulletSpeed;
}
