﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BulletHellSystem : MonoBehaviour
{
    [Header("SHOOTPOINT DISTANCE FROM ORIGIN")]
    [SerializeField]
    private float m_distance;

    [Header("MODULO WAVE")]
    [SerializeField]
    private int m_moduloDivider;
    [SerializeField]
    private int m_moduloStepIncrease;

    [Header("LINE WAVE")]
    [SerializeField]
    private int m_sizeOfLine;
    [SerializeField]
    private float m_bulletDistance;

    [Header("ARC WAVE")]
    [SerializeField]
    private int m_arcLength;

    [Header("SINE WAVE")]
    [SerializeField]
    private int m_sineSpeed;

    [Header("WAVES")]
    [SerializeField]
    private List<Wave> m_waveList = new List<Wave>();

    private List<Transform> m_shootPoints;
    private IEnumerator m_performBulletHell;
    private int m_currentWave = 0;
    private bool m_isBulletHellRunning;
    private Transform m_target;

    private ObjectPool m_bulletPool;

    #region PRIVATE

    private void Awake()
    {
        m_bulletPool = GetComponent<ObjectPool>();
    }

    private void InstantiatePoints(int number)
    {
        for (int i = 0; i < number; i++)
        {
            int nr = i + 1;
            var shootPoint = new GameObject
            {
                name = "shootPoint" + nr
            };
            shootPoint.transform.SetParent(this.transform);
            m_shootPoints.Add(shootPoint.transform);
        }
    }

    private void CalculatePositions(Vector3 position, int number)
    {
        for (int i = 0; i < number; i++)
        {
            float pointAngle = 2f * Mathf.PI / (float)number * i;
            Vector2 currentPoint = new Vector2(Mathf.Cos(pointAngle) * m_distance, Mathf.Sin(pointAngle) * m_distance)
                + new Vector2(position.x, position.y);
            m_shootPoints[i].position = currentPoint;
        }
    }

    private void DestroyPoints()
    {
        for (int i = 0; i < m_shootPoints.Count; i++)
        {
            Destroy(m_shootPoints[i]);
        }
        m_shootPoints.Clear();
    }

    private void CreateList()
    {
        if (m_shootPoints == null)
            m_shootPoints = new List<Transform>();
    }

    private Transform FindClosestPointToTarget()
    {
        float shortest = float.MaxValue;
        Transform trans = null;
        foreach (Transform p in m_shootPoints)
        {
            var pos = m_target.position;
            var dist = Vector3.Distance(p.position, pos);
            if (dist < shortest)
            {
                shortest = dist;
                trans = p;
            }
        }
        return trans;
    }

    private int FindPointInList(Transform point)
    {
        int a = 0;
        for (int i = 0; i < m_shootPoints.Count; i++)
        {
            if (m_shootPoints[i].name.Equals(point.name))
            {
                a = i;
            }
        }
        return a;
    }

    private List<Transform> CreatePointsForward(int pos)
    {
        List<Transform> newList = new List<Transform>();
        for (int i = 0; i < m_arcLength / 2; i++)
        {
            pos++;
            if (pos > m_shootPoints.Count - 1)
                pos = 0;
            newList.Add(m_shootPoints.ElementAt(pos));
        }
        return newList;
    }

    private List<Transform> CreatePointsBackward(int pos)
    {
        List<Transform> newList = new List<Transform>();
        for (int i = 0; i < m_arcLength / 2; i++)
        {
            pos--;
            if (pos < 0)
                pos = m_shootPoints.Count - 1;
            newList.Add(m_shootPoints.ElementAt(pos));
        }
        return newList;
    }

    private GameObject CreateBullet(Wave w, Vector3 target, Transform trans, bool isNotSingleShootPoint = true)
    {
        //var obj = Instantiate(w.m_projectile, trans.position, trans.rotation);
        var obj = m_bulletPool.GetObjectFromDictionary(0, trans.position, trans.rotation);
        obj.transform.parent = null;
        var bullet = obj.GetComponent<Bullet>();
        Vector3 heading;
        if (isNotSingleShootPoint)
            heading = target - transform.position;
        else
            heading = target - trans.position;
        var direction = heading.normalized;
        obj.transform.right = direction;
        bullet.SetOwner(this.gameObject);
        bullet.SetDamage(w.m_bulletDamage);
        bullet.SetMoveSpeed(w.m_bulletSpeed);
        bullet.SetDirection(direction);
        return obj;
    }

    private void AddAdditionalBullets(GameObject firstBullet, Transform trans, Vector3 target, Wave w, float dist)
    {
        var firstBulletPosition = firstBullet.transform.localPosition;
        var distance = dist;
        for (int j = 0; j < m_sizeOfLine; j++)
        {
            var obj = Instantiate(w.m_projectile);
            //var objs = m_bulletPool.GetObjectFromDictionary(0);
            obj.transform.parent = firstBullet.transform;
            obj.transform.localPosition = new Vector3(0, firstBulletPosition.y + distance, 0);
            distance += dist;
            var newBullet = obj.GetComponent<Bullet>();
            var heading = target - trans.position;
            var direction = heading.normalized;
            obj.transform.right = direction;
            obj.transform.parent = null;
            newBullet.SetOwner(this.gameObject);
            newBullet.SetDamage(w.m_bulletDamage);
            newBullet.SetMoveSpeed(w.m_bulletSpeed);
            newBullet.SetDirection(direction);
        }
    }

    private IEnumerator GetBulletHell(string hell)
    {
        if (hell.Equals("normal"))
            m_performBulletHell = Normal();
        else if (hell.Equals("modulo"))
            m_performBulletHell = Modulo();
        else if (hell.Equals("single"))
            m_performBulletHell = Single();
        else if (hell.Equals("line"))
            m_performBulletHell = Line();
        else if (hell.Equals("arc"))
            m_performBulletHell = Arc();

        return m_performBulletHell;
    }

    private IEnumerator Normal()
    {
        m_isBulletHellRunning = true;
        Wave w = m_waveList.ElementAt(m_currentWave);
        for (int i = 0; i < w.m_counter; i++)
        {
            for (int j = 0; j < m_shootPoints.Count; j++)
            {
                CreateBullet(w, m_shootPoints.ElementAt(j).position, m_shootPoints.ElementAt(j));
            }
            yield return new WaitForSecondsRealtime(w.m_delay);
        }
        m_isBulletHellRunning = false;
    }

    private IEnumerator Modulo()
    {
        m_isBulletHellRunning = true;
        Wave w = m_waveList.ElementAt(m_currentWave);
        int step = 0;
        for (int i = 0; i < w.m_counter; i++)
        {
            for (int j = 0; j < m_shootPoints.Count; j++)
            {
                if ((j + step) % m_moduloDivider == 0)
                {
                    CreateBullet(w, m_shootPoints.ElementAt(j).position, m_shootPoints.ElementAt(j));
                }
            }
            // Another solution is to set j to step and increase it by m_moduloDivider.
            step += m_moduloStepIncrease;
            yield return new WaitForSecondsRealtime(w.m_delay);
        }
        m_isBulletHellRunning = false;
    }

    private IEnumerator Single()
    {
        m_isBulletHellRunning = true;
        Wave w = m_waveList.ElementAt(m_currentWave);
        var shootPoint = FindClosestPointToTarget();
        for (int i = 0; i < w.m_counter; i++)
        {
            CreateBullet(w, m_target.position, shootPoint, false);
            yield return new WaitForSecondsRealtime(w.m_delay);
        }
        m_isBulletHellRunning = false;
    }

    private IEnumerator Line()
    {
        m_isBulletHellRunning = true;
        Wave w = m_waveList.ElementAt(m_currentWave);
        var shootPoint = FindClosestPointToTarget();
        var playerPosition = m_target.position;
        for (int i = 0; i < w.m_counter; i++)
        {
            var obj = CreateBullet(w, playerPosition, shootPoint, false);
            AddAdditionalBullets(obj, shootPoint, playerPosition, w, m_bulletDistance);
            AddAdditionalBullets(obj, shootPoint, playerPosition, w, -m_bulletDistance);
            yield return new WaitForSecondsRealtime(w.m_delay);
        }
        m_isBulletHellRunning = false;
    }

    private IEnumerator Arc()
    {
        m_isBulletHellRunning = true;
        Wave w = m_waveList.ElementAt(m_currentWave);
        var shootPoint = FindClosestPointToTarget();
        var pos = FindPointInList(shootPoint);
        List<Transform> newList = new List<Transform>
        {
            shootPoint
        };
        List<Transform> forwardList = CreatePointsForward(pos);
        List<Transform> backwardList = CreatePointsBackward(pos);
        newList.AddRange(forwardList);
        newList.AddRange(backwardList);
        for (int i = 0; i < w.m_counter; i++)
        {
            for (int j = 0; j < newList.Count; j++)
            {
                CreateBullet(w, newList.ElementAt(j).position, newList.ElementAt(j));
            }
            yield return new WaitForSecondsRealtime(w.m_delay);
        }
        m_isBulletHellRunning = false;
    }

    #endregion

    #region PUBLIC

    // To create new points outside of this class.
    public void CreateShootPoints(Vector3 pos, int num, bool destroy = false)
    {
        CreateList();
        if (destroy)
            DestroyPoints();
        InstantiatePoints(num);
        CalculatePositions(pos, num);
    }

    // To activate the bullet hell outside of this class.
    public void BulletHell(string hell, bool increase = false)
    {
        if (m_performBulletHell != null && !m_isBulletHellRunning)
            StopCoroutine(m_performBulletHell);
        m_performBulletHell = GetBulletHell(hell);
        if (!m_isBulletHellRunning)
            StartCoroutine(m_performBulletHell);
        if (increase && m_currentWave < m_waveList.Count - 1)
            m_currentWave++;
    }

    public void SetTarget(GameObject target)
    {
        m_target = target.transform;
    }

    public bool BulletHellRunning()
    {
        return m_isBulletHellRunning;
    }

    #endregion
}
