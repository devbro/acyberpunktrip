﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    private string playerName;
    private float health;
    private GameObject weapon;
    public Vector2 position;

    public PlayerData(Vector2 position)
    {
        this.position = position;
    }

}
