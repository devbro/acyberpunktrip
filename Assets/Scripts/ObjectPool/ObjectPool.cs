﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [SerializeField]
    private GameObject[] m_objectsToPool;
    [SerializeField]
    private int m_numberOfObjects;
    private Dictionary<int, List<GameObject>> m_allObjects = new Dictionary<int, List<GameObject>>();

    private void Awake()
    {
        CreateObjectDictionary();
    }

    private void CreateObjectDictionary()
    {
        for (int arrayIndex = 0; arrayIndex < m_objectsToPool.Length; arrayIndex++)
        {
            List<GameObject> dictionaryList = new List<GameObject>();
            for (int i = 0; i < m_numberOfObjects; i++)
            {
                var item = Instantiate(m_objectsToPool[arrayIndex], transform.position, Quaternion.identity);
                item.SetActive(false);
                dictionaryList.Add(item);
            }
            m_allObjects.Add(arrayIndex, dictionaryList);
        }
    }

    private GameObject CreateMoreObjects(int id)
    {
        GameObject item = null;
        if (m_allObjects.ContainsKey(id))
        {
            for (int i = 0; i < m_numberOfObjects; i++)
            {
                item = Instantiate(m_objectsToPool[id], transform.position, Quaternion.identity);
                item.SetActive(false);
                m_allObjects[id].Add(item);
            }
        }
        return item;
    }

    public GameObject GetObjectFromDictionary(int id, Vector3 position, Quaternion rotation)
    {
        GameObject item = null;
        if (m_allObjects.ContainsKey(id))
        {
            foreach (GameObject itemToGet in m_allObjects[id])
            {
                if (!itemToGet.activeSelf)
                {
                    item = itemToGet;
                    break;
                }
            }
            if (item == null)
                item = CreateMoreObjects(id);
            item.SetActive(true);
            item.transform.position = position;
            item.transform.rotation = rotation;
        }
        return item;
    }

    public GameObject GetObjectFromDictionary(int id)
    {
        GameObject item = null;
        if (m_allObjects.ContainsKey(id))
        {
            List<GameObject> list = m_allObjects[id];
            foreach (GameObject itemToGet in list)
            {
                if (!itemToGet.activeSelf)
                {
                    item = itemToGet;
                    break;
                }
            }
            item.SetActive(true);
        }
        return item;
    }

    public int GetFromObjectArray()
    {
        return Random.Range(0, m_objectsToPool.Length);
    }
}
