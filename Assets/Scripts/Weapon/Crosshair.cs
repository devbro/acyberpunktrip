﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosshair : MonoBehaviour
{
    [SerializeField]
    private LayerMask m_searchLayer;
    public Collider2D FindTarget()
    {
        Collider2D hit = Physics2D.OverlapCircle(transform.position, 1, m_searchLayer);
        if (hit != null)
        {
            return hit;
        }
        else
            return null;
    }
}
