﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialAbilities : MonoBehaviour
{
    [Header("DEAD EYE")]
    public RangedWeapon DeadEye;
    public float DeadEyeSpeed;

    [Header("SWORD DANCE")]
    public MeleeWeapon SwordDance;
    public float SwordDanceSpeed;

    [System.Serializable]
    public struct DeadEyeStats
    {

    }

    [System.Serializable]
    public struct SwordDanceStats
    {
        public float speed;
    }
}
