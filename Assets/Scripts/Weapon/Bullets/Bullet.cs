﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField]
    private int m_setInactiveDistance;
    private float m_bulletSpeed;
    private Vector3 m_direction;
    private float m_damage;
    private GameObject m_owner;
    private string m_ownerName;

    private Vector3 m_startPos;
    private Vector2 m_crossDirection;

    private void Awake()
    {
        m_startPos = transform.position;
    }

    private void Update()
    {
        BulletMovement();
        CheckDistance();
    }

    private void BulletMovement()
    {
        // FOR SOME REASON YOU HAVE TO NORMALIZE THE TRANSLATION VECTOR IN TRANSFORM.TRANSLATE!!!
        transform.Translate((new Vector2(m_direction.x, m_direction.y).normalized * (m_bulletSpeed * Time.deltaTime)), Space.World);

        //var test = Mathf.Sin(Time.time * 111);
        //transform.Translate(new Vector2(m_direction.x, test), Space.World);
    }

    public void SetDirection(Vector3 direction)
    {
        m_direction = direction;
        //m_crossDirection = new Vector2(direction.y, -direction.x);
    }

    public void SetDamage(float damage)
    {
        m_damage = damage;
    }

    public void SetOwner(GameObject owner)
    {
        m_owner = owner;
        m_ownerName = owner.name;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<IDamageable>() != null)
        {
            if (m_owner == null)
                return;
            else if (m_owner == collision.gameObject)
                return;
            else if (m_owner.CompareTag("Enemy") && collision.gameObject.CompareTag("Enemy"))
                return;
            else if (m_owner.CompareTag("EnemyBullet") && collision.gameObject.CompareTag("EnemyBullet"))
                return;
            var target = collision.GetComponent<IDamageable>();
            target.ApplyDamage(m_damage, m_ownerName);
            gameObject.SetActive(false);
        }
        else
            return;
    }

    public string GetOwnerByName()
    {
        return m_ownerName;
    }

    public void CheckDistance()
    {
        if (m_owner != null)
        {
            var dist = Vector3.Distance(transform.position, m_owner.transform.position);
            int distToInt = (int)dist;
            if (distToInt > m_setInactiveDistance)
            {
                gameObject.SetActive(false);
            }
        }
    }

    #region Interface

    public void SetMoveSpeed(float movespeed)
    {
        m_bulletSpeed = movespeed;
    }

    #endregion
}
