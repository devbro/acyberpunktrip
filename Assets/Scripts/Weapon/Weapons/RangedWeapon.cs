﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "rangedWeapon", menuName = "ScriptableObjects/RangedWeapon")]
public class RangedWeapon : Weapon
{
    public GameObject projectile;
    [SerializeField]
    private float projectileSpeed;
    private float originalProjectileSpeed;

    public void SetProjectileSpeed(float speed)
    {
        projectileSpeed = speed;
    }

    public void SetOriginalProjectileSpeed(float speed)
    {
        originalProjectileSpeed = speed;
    }

    public float GetProjectileSpeed()
    {
        return projectileSpeed;
    }

    public float GetOriginalProjectileSpeed()
    {
        return originalProjectileSpeed;
    }
}
