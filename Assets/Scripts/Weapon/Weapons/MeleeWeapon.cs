﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "meleeWeapon", menuName = "ScriptableObjects/MeleeWeapon")]
public class MeleeWeapon : Weapon
{
    public float attackRadius;
}
