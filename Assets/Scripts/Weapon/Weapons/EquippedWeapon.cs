﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquippedWeapon : MonoBehaviour
{
    [Header("Start Weapons")]
    [SerializeField]
    private RangedWeapon m_rangedWeapon;
    [SerializeField]
    private MeleeWeapon m_meleeWeapon;

    [Header("Equipped Weapons")]
    [HideInInspector]
    public RangedWeapon EquippedRangedWeapon;
    [HideInInspector]
    public MeleeWeapon EquippedMeleeWeapon;

    private float m_lastAttack = 0.0f;

    private ObjectPool m_bulletPool;

    private void Awake()
    {
        if (m_rangedWeapon != null)
            EquippedRangedWeapon = m_rangedWeapon;
        if (m_meleeWeapon != null)
            EquippedMeleeWeapon = m_meleeWeapon;
        m_bulletPool = GetComponent<ObjectPool>();
    }

    public void RangedAttack(Vector3 startPos, Quaternion startRot, GameObject target)
    {
        // This implementation of fire rate does have a delay at the first shot.
        if (Time.time > EquippedRangedWeapon.attackRate + m_lastAttack)
        {
            var bulletObject = m_bulletPool.GetObjectFromDictionary(0, startPos, startRot);
            var bullet = bulletObject.GetComponent<Bullet>();
            var heading = target.transform.position - startPos;
            var distance = heading.magnitude;
            var direction = heading / distance;
            //bulletObject.transform.right = direction; // ADDED THIS 201105 IF BULLETS STARTS TO ACT WEIRD.

            bullet.SetDamage(EquippedRangedWeapon.damage);
            bullet.SetOwner(this.gameObject);
            bullet.SetMoveSpeed(EquippedRangedWeapon.GetProjectileSpeed());
            bullet.SetDirection(direction);
            m_lastAttack = Time.time;
        }
    }

    public void MeleeAttack(Collider2D[] targets)
    {
        if (Time.time > EquippedMeleeWeapon.attackRate + m_lastAttack)
        {
            foreach (Collider2D target in targets)
            {
                var enemy = target.GetComponent<IDamageable>();
                if (enemy != null)
                    enemy.ApplyDamage(EquippedMeleeWeapon.damage, gameObject.name);
            }
            m_lastAttack = Time.time;
        }
    }

    //To check melee weaon attack radius
    //void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.yellow;
    //    Gizmos.DrawSphere(testPoint.transform.position, EquippedMeleeWeapon.attackRadius);
    //}

    public RangedWeapon GetOriginalRangedWeapon()
    {
        return m_rangedWeapon;
    }

    public MeleeWeapon GetOriginalMeleeWeapon()
    {
        return m_meleeWeapon;
    }

}
