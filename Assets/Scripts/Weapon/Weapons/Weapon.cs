﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : ScriptableObject
{
    public string weaponName;
    public Sprite sprite;
    public float attackRate;
    public float damage;
}
