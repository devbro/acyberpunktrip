﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyInterface : InterfaceHandler
{
    [SerializeField]
    private float m_offset;

    protected override void Start()
    {
        base.Start();
        SetupEnemySlider();
    }

    private void Update()
    {
        m_slider.transform.position = Camera.main.WorldToScreenPoint(transform.position + new Vector3(0, m_offset, 0));
    }

    private void UpdateHealthSlider()
    {
        m_slider.gameObject.SetActive(m_slider.value < m_slider.maxValue);
    }

    private void SetupEnemySlider()
    {
        m_slider.gameObject.SetActive(false);
        m_entity.UpdateCurrentHealth += UpdateHealthSlider;
    }
}
