﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BossInterface : InterfaceHandler
{
    [SerializeField]
    private TextMeshProUGUI text;

    private Boss m_boss;
    private string m_string;

    protected override void Start()
    {
        base.Start();
        m_boss = GetComponent<Boss>();
        if (m_boss != null)
        {
            m_string = m_boss.Name();
            text.text = m_string;
        }
        SetInterface(false);
    }

    public void SetInterface(bool enable)
    {
        m_slider.gameObject.SetActive(enable);
        text.gameObject.SetActive(enable);
    }
}
