﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceHandler : MonoBehaviour
{
    protected Entity m_entity;
    [SerializeField]
    protected Slider m_slider;
    [SerializeField]
    private Color m_low;
    [SerializeField]
    private Color m_high;

    protected virtual void Start()
    {
        GetEntity();
    }

    private void GetEntity()
    {
        m_entity = GetComponent<Entity>();
        if (m_entity != null)
        {
            m_entity.UpdateCurrentHealth += UpdateHealth;
            m_slider.minValue = 0;
            m_slider.maxValue = m_entity.GetCurrentHealth();
            m_slider.value = m_slider.maxValue;
            m_slider.fillRect.GetComponentInChildren<Image>().color = m_high;
        }
    }

    private void UpdateHealth()
    {
        m_slider.value = m_entity.GetCurrentHealth(); ;
        m_slider.fillRect.GetComponentInChildren<Image>().color = Color.Lerp(m_low, m_high, m_slider.normalizedValue);
    }
}
